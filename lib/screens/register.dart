import 'dart:convert';
import 'dart:io';
import 'dart:developer' as developer;
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter/material.dart';
import 'package:lobby/components/navigation_drawer.dart';
import 'package:http/http.dart' as http;
import 'package:lobby/localization/localization_const.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key key}) : super(key: key);

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  final _formKey = GlobalKey<FormState>();

  final _usernameController = TextEditingController();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  final _passwordConfirmController = TextEditingController();

  bool _obscureText = true;
  bool _usernameAvailable = true;
  bool _emailAvailable = true;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.centerLeft,
          end: Alignment.centerRight,
          colors: [
            Color(0xffFF512F),
            Color(0xffDD2475),
          ],
        ),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        drawer: const NavigationDrawer(),
        body: SingleChildScrollView(
          child: Container(
            padding: const EdgeInsets.all(20),
            child: Padding(
              padding: EdgeInsets.only(
                top: MediaQuery.of(context).size.height * 0.05,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.05,
                  ),
                  Form(
                    key: _formKey,
                    child: Column(
                      children: [
                        TextFormField(
                          style: const TextStyle(color: Colors.white),
                          controller: _usernameController,
                          onChanged: (value) {
                            // TODO: Debounce
                            // Ignore invalid values
                            if (value.length < 3 ||
                                value.length > 30 ||
                                !RegExp(r'^[a-z0-9]+$').hasMatch(value)) {
                              return;
                            }

                            _isUsernameUnique(value);
                          },
                          decoration: InputDecoration(
                            // Username not available indicator
                            suffixIcon: !_usernameAvailable
                                ? const Icon(
                                    Icons.close,
                                  )
                                : null,
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(7),
                              borderSide: BorderSide.none,
                            ),
                            focusedBorder: const OutlineInputBorder(
                              borderSide: BorderSide(
                                width: 1,
                                color: Color.fromRGBO(
                                  255,
                                  255,
                                  255,
                                  0.6,
                                ),
                              ),
                            ),
                            filled: true,
                            fillColor: const Color.fromRGBO(255, 255, 255, 0.3),
                            prefixIcon: const Icon(
                              Icons.person_rounded,
                              color: Colors.white,
                            ),
                            labelStyle: const TextStyle(
                              color: Colors.white,
                            ),
                            hintStyle: const TextStyle(
                              color: Colors.white,
                            ),
                            labelText: getTranslated(context, 'username'),
                            hintText: getTranslated(context, 'enter_username'),
                            errorStyle: const TextStyle(
                              color: Colors.white,
                            ),
                          ),
                          validator: (value) {
                            if (value.length < 3 || value.length > 30) {
                              return getTranslated(context, 'between_3_20');
                            }

                            if (!RegExp(r'^[a-z0-9]+$').hasMatch(value)) {
                              return getTranslated(
                                      context, 'contain_lowercase') +
                                  getTranslated(context, 'letters_numbers');
                            }

                            return null;
                          },
                        ),
                        const SizedBox(height: 20),
                        TextFormField(
                          style: const TextStyle(color: Colors.white),
                          controller: _emailController,
                          onChanged: (value) {
                            // TODO: Debounce
                            // Ignore invalid values
                            if (!value.contains('@')) {
                              return;
                            }

                            _isEmailUnique(value);
                          },
                          decoration: InputDecoration(
                            suffixIcon: !_emailAvailable
                                ? const Icon(
                                    Icons.close,
                                  )
                                : null,
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(7),
                              borderSide: BorderSide.none,
                            ),
                            focusedBorder: const OutlineInputBorder(
                              borderSide: BorderSide(
                                width: 1,
                                color: Color.fromRGBO(
                                  255,
                                  255,
                                  255,
                                  0.6,
                                ),
                              ),
                            ),
                            filled: true,
                            fillColor: const Color.fromRGBO(255, 255, 255, 0.3),
                            prefixIcon: const Icon(
                              Icons.person_rounded,
                              color: Colors.white,
                            ),
                            labelStyle: const TextStyle(
                              color: Colors.white,
                            ),
                            hintStyle: const TextStyle(
                              color: Colors.white,
                            ),
                            labelText: getTranslated(context, 'email'),
                            hintText: getTranslated(context, 'enter_email'),
                            errorStyle: const TextStyle(
                              color: Colors.white,
                            ),
                          ),
                          validator: (value) {
                            if (!value.contains('@')) {
                              return getTranslated(context, 'contain_sign');
                            }
                            return null;
                          },
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        TextFormField(
                          style: const TextStyle(color: Colors.white),
                          controller: _passwordController,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(7),
                              borderSide: BorderSide.none,
                            ),
                            focusedBorder: const OutlineInputBorder(
                                borderSide: BorderSide(
                              width: 1,
                              color: Color.fromRGBO(
                                255,
                                255,
                                255,
                                0.6,
                              ),
                            )),
                            filled: true,
                            fillColor: const Color.fromRGBO(255, 255, 255, 0.3),
                            prefixIcon: const Icon(
                              Icons.lock,
                              color: Colors.white,
                            ),
                            suffixIcon: GestureDetector(
                              onTap: () {
                                setState(() {
                                  _obscureText = !_obscureText;
                                });
                              },
                              child: Icon(
                                _obscureText
                                    ? Icons.visibility_off
                                    : Icons.visibility,
                                color: const Color.fromRGBO(255, 255, 255, 0.8),
                              ),
                            ),
                            labelStyle: const TextStyle(
                              color: Colors.white,
                            ),
                            hintStyle: const TextStyle(
                              color: Colors.white,
                            ),
                            labelText: getTranslated(context, 'password'),
                            hintText: getTranslated(context, 'enter_password'),
                            errorStyle: const TextStyle(
                              color: Colors.white,
                            ),
                          ),
                          obscureText: _obscureText,
                          validator: (value) {
                            if (value.length < 5) {
                              return getTranslated(context, 'five_characters');
                            }
                            return null;
                          },
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        TextFormField(
                          style: const TextStyle(color: Colors.white),
                          controller: _passwordConfirmController,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(7),
                              borderSide: BorderSide.none,
                            ),
                            focusedBorder: const OutlineInputBorder(
                                borderSide: BorderSide(
                              width: 1,
                              color: Color.fromRGBO(
                                255,
                                255,
                                255,
                                0.6,
                              ),
                            )),
                            filled: true,
                            fillColor: const Color.fromRGBO(255, 255, 255, 0.3),
                            prefixIcon: const Icon(
                              Icons.lock,
                              color: Colors.white,
                            ),
                            suffixIcon: GestureDetector(
                              onTap: () {
                                setState(() {
                                  _obscureText = !_obscureText;
                                });
                              },
                              child: Icon(
                                _obscureText
                                    ? Icons.visibility_off
                                    : Icons.visibility,
                                color: const Color.fromRGBO(255, 255, 255, 0.8),
                              ),
                            ),
                            labelStyle: const TextStyle(
                              color: Colors.white,
                            ),
                            hintStyle: const TextStyle(
                              color: Colors.white,
                            ),
                            labelText:
                                getTranslated(context, 'password_confirm'),
                            hintText:
                                getTranslated(context, 'reenter_password'),
                            errorStyle: const TextStyle(
                              color: Colors.white,
                            ),
                          ),
                          obscureText: _obscureText,
                          validator: (value) {
                            if (value != _passwordController.text) {
                              return getTranslated(context, 'same_as_password');
                            }
                            return null;
                          },
                        ),
                        const SizedBox(
                          height: 25,
                        ),
                        ElevatedButton(
                          style: ButtonStyle(
                            shape: MaterialStateProperty.all<
                                RoundedRectangleBorder>(
                              RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30),
                              ),
                            ),
                            minimumSize: MaterialStateProperty.all(
                              const Size(200, 50),
                            ),
                            backgroundColor: MaterialStateProperty.all(
                              Colors.white,
                            ),
                          ),
                          onPressed: (_usernameAvailable && _emailAvailable)
                              ? () {
                                  if (_formKey.currentState.validate()) {
                                    _submitForm();
                                  }
                                }
                              : null,
                          child: Padding(
                            padding: const EdgeInsets.only(top: 10, bottom: 10),
                            child: Text(
                              getTranslated(context, "confirm"),
                              style: const TextStyle(
                                color: Color(0xffFF512F),
                                fontSize: 20,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void _submitForm() async {
    try {
      const storage = FlutterSecureStorage();

      final String apiKey =
          await storage.read(key: dotenv.env['BACKEND_TOKEN_KEY']);

      final http.Response response = await http.post(
        Uri.http(dotenv.env['API_URL'], '/users/'),
        headers: {
          HttpHeaders.authorizationHeader: 'bearer $apiKey',
          HttpHeaders.contentTypeHeader: 'application/json; charset=UTF-8',
        },
        body: jsonEncode(
          <String, String>{
            'username': _usernameController.text,
            'email': _emailController.text,
            'password': _passwordController.text,
          },
        ),
      );

      if (response.statusCode == 200) {
        if (!mounted) return;

        Navigator.pushReplacementNamed(context, 'login');
      } else if (response.statusCode == 400) {
        // TODO: Check what kind of error we have

        if (jsonDecode(response.body)['validationErrors'] != null) {
          // TODO: Show dialog
          developer.log(
            'Validation errors',
            name: 'register',
          );
        }
      }
    } catch (err) {
      developer.log(
        'Connection error',
        name: 'register',
        error: err,
      );
    }
  }

  void _isUsernameUnique(String username) async {
    try {
      const storage = FlutterSecureStorage();

      final String apiKey =
          await storage.read(key: dotenv.env['BACKEND_TOKEN_KEY']);

      final http.Response response = await http.post(
        Uri.http(dotenv.env['API_URL'], '/users/'),
        headers: {
          HttpHeaders.authorizationHeader: 'bearer $apiKey',
          HttpHeaders.contentTypeHeader: 'application/json; charset=UTF-8',
        },
        body: jsonEncode(<String, String>{'username': username}),
      );

      if (response.statusCode == 200) {
        final data = jsonDecode(response.body);

        setState(() {
          _usernameAvailable = (data == null);
        });
      }
    } catch (err) {
      developer.log(
        'Connection error',
        name: 'register',
        error: err,
      );
    }
  }

  void _isEmailUnique(String email) async {
    try {
      const storage = FlutterSecureStorage();

      final String apiKey =
          await storage.read(key: dotenv.env['BACKEND_TOKEN_KEY']);

      final http.Response response = await http.post(
        Uri.http(dotenv.env['API_URL'], '/users/'),
        headers: {
          HttpHeaders.authorizationHeader: 'bearer $apiKey',
          HttpHeaders.contentTypeHeader: 'application/json; charset=UTF-8',
        },
        body: jsonEncode(<String, String>{'email': email}),
      );

      if (response.statusCode == 200) {
        final data = jsonDecode(response.body);

        setState(() {
          _emailAvailable = (data == null);
        });
      }
    } catch (err) {
      developer.log(
        'Connection error',
        name: 'register',
        error: err,
      );
    }
  }
}
