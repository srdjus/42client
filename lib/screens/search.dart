import 'dart:convert';
import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_dotenv/flutter_dotenv.dart';

class SearchScreen extends StatefulWidget {
  const SearchScreen({Key key}) : super(key: key);

  @override
  State<SearchScreen> createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  Future<http.Response> _response;
  Timer _debounce;

  // Search query invalid
  bool queryInvalid = false;

  void _onSearchChanged(String value) {
    if (_debounce?.isActive ?? false) _debounce.cancel();

    _debounce = Timer(
      const Duration(milliseconds: 500),
      () {
        if (value.length < 3 || value.length > 30) {
          setState(() {
            // Just to invoke the future builder states
            queryInvalid = true;
            _response = Future.value();
          });
        } else {
          setState(() {
            queryInvalid = false;
            _response = _fetchUsers(value);
          });
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.centerLeft,
          end: Alignment.centerRight,
          colors: [
            Color(0xffFF512F),
            Color(0xffDD2475),
          ],
        ),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          shadowColor: Colors.transparent,
          title: TextField(
            onChanged: _onSearchChanged,
            style: const TextStyle(
              color: Colors.white,
              fontSize: 18,
              letterSpacing: -1,
            ),
            decoration: const InputDecoration(
              hintText: 'Enter username',
              hintStyle: TextStyle(
                color: Colors.white,
              ),
              enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(
                  color: Color(0xffFF512F),
                  style: BorderStyle.solid,
                ),
              ),
              prefixIcon: Icon(
                Icons.search,
                color: Colors.white,
              ),
            ),
          ),
        ),
        body: FutureBuilder(
          future: _response,
          builder: (
            BuildContext context,
            AsyncSnapshot<http.Response> snapshot,
          ) {
            if (snapshot.hasData) {
              if (snapshot.data.statusCode == 200) {
                // TODO:  Wrap user search in class
                final Map user = jsonDecode(snapshot.data.body);

                if (user == null) {
                  return Container(
                    margin: const EdgeInsets.only(
                      left: 20,
                      top: 20,
                    ),
                    child: const Text(
                      'No results.',
                      style: TextStyle(
                        fontSize: 18,
                        color: Colors.white,
                        letterSpacing: -1,
                      ),
                    ),
                  );
                }

                return Container(
                  padding: const EdgeInsets.symmetric(
                    vertical: 0,
                    horizontal: 20,
                  ),
                  // TODO: User listbuilder and list all users that match a query
                  child: _buildResultTile(user),
                );
              }
              // Don't show loading for Future.value() futures
            } else if (snapshot.connectionState == ConnectionState.waiting &&
                !queryInvalid) {
              return Container(
                alignment: Alignment.center,
                child: const CircularProgressIndicator(
                  color: Colors.white,
                ),
              );
            }

            // General case
            return Container(
              margin: const EdgeInsets.only(
                left: 20,
                top: 20,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: const [
                  Text(
                    'Suggested people',
                    style: TextStyle(
                      fontSize: 18,
                      color: Colors.white,
                      letterSpacing: -0.5,
                    ),
                  ),
                  Text(
                    'Currently no suggestions.',
                    style: TextStyle(
                      color: Color(0xFFEEEEEE),
                      letterSpacing: -0.5,
                    ),
                  )
                ],
              ),
            );
          },
        ),
      ),
    );
  }

  Widget _buildResultTile(user) {
    // TODO: Build different subheading, implement matches
    String subheading = '';

    if (user['location'] != null) {
      subheading = 'From ${user['location']}';
    } else if (user.containsKey('interests')) {
      subheading = 'Interested in:';

      // Show max 3 interests
      int max = user['interests'].length < 3 ? user['interests'].length : 3;

      for (int i = 0; i < max; i++) {
        subheading += ' ${user['interests'][i]}';
        if (i < max - 1) {
          subheading += ',';
        } else if (user['interests'].length > max) {
          subheading += '...';
        }
      }
    } else {
      subheading = '12 matches';
    }

    return Container(
      margin: const EdgeInsets.only(top: 10),
      height: 70,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.1),
            spreadRadius: 1,
            blurRadius: 2,
            offset: const Offset(0, 2),
          ),
        ],
      ),
      child: Row(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(5),
            child: Image.asset(
              'assets/images/avatars/${user['avatar']}.png',
              fit: BoxFit.cover,
            ),
          ),
          Expanded(
            child: Container(
              margin: const EdgeInsets.only(
                left: 20,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    user['username'],
                    style: const TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  Text(
                    subheading,
                    style: const TextStyle(
                      fontSize: 13,
                      color: Color(0xFF777777),
                    ),
                  ),
                ],
              ),
            ),
          ),
          IconButton(
            onPressed: () {
              Navigator.of(context).pushNamed(
                'profile',
                arguments: {
                  'username': user['username'],
                },
              );
            },
            color: const Color(0xffDD2475),
            icon: const Icon(
              Icons.arrow_forward,
            ),
          ),
        ],
      ),
    );
  }

  Future<http.Response> _fetchUsers(String username) async {
    const storage = FlutterSecureStorage();

    final String apiKey =
        await storage.read(key: dotenv.env['BACKEND_TOKEN_KEY']);

    return http.get(
      Uri.http(dotenv.env['API_URL'], '/users/', {'username': username}),
      headers: {HttpHeaders.authorizationHeader: 'bearer $apiKey'},
    );
  }

  @override
  void dispose() {
    _debounce?.cancel();
    super.dispose();
  }
}
