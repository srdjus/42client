import 'dart:convert';
import 'dart:io';
import 'dart:developer' as developer;
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:lobby/components/curved_app_bar.dart';
import 'package:lobby/components/navigation_drawer.dart';
import 'package:http/http.dart' as http;
import 'package:lobby/localization/localization_const.dart';
import 'package:lobby/models/session.dart';
import 'package:lobby/screens/lobby.dart';
import 'package:provider/provider.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:flutter_dotenv/flutter_dotenv.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key key}) : super(key: key);

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  final _postTextController = TextEditingController();
  final _postFormKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    Map args = ModalRoute.of(context).settings.arguments;

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: CurvedAppBar(
        title: getTranslated(context, 'profile'),
        icon: Icons.person,
      ),
      drawer: const NavigationDrawer(),
      body: Container(
        decoration: const BoxDecoration(
          color: Colors.white,
        ),
        child: FutureBuilder(
          future: () async {
            const storage = FlutterSecureStorage();

            final String apiKey =
                await storage.read(key: dotenv.env['BACKEND_TOKEN_KEY']);

            return http.get(
              Uri.http(
                dotenv.env['API_URL'],
                '/users/${args['username']}',
              ),
              headers: {HttpHeaders.authorizationHeader: 'bearer $apiKey'},
            );
          }(),
          builder: (
            BuildContext context,
            AsyncSnapshot<http.Response> snapshot,
          ) {
            if (snapshot.hasData && snapshot.data.statusCode == 200) {
              return _buildProfileInfo(snapshot.data.body);
            } else if (snapshot.hasError) {
              // TODO: This is awful, show error screen instead
              return Text(getTranslated(context, 'have_error'));
            } else {
              return Column(
                children: const [
                  SizedBox(
                    width: 60,
                    height: 60,
                    child: CircularProgressIndicator(),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 16),
                    child: Text('Loading...'),
                  ),
                ],
              );
            }
          },
        ),
      ),
    );
  }

  Widget _buildProfileInfo(String data) {
    Map user = jsonDecode(data);
    return Consumer<SessionModel>(builder: (context, session, child) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                width: 118,
                child: Stack(
                  children: [
                    Positioned(
                      child: Container(
                        width: 90,
                        height: 90,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(
                            color: const Color(0xffFF512F),
                            width: 2,
                          ),
                          image: DecorationImage(
                            fit: BoxFit.fill,
                            image: AssetImage(
                              'assets/images/avatars/${user["avatar"]}.png',
                            ),
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      right: 20,
                      top: 40,
                      child: Container(
                        width: 40,
                        height: 40,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.5),
                              offset: const Offset(0.0, 0.5),
                              blurRadius: 3.0,
                            ),
                          ],
                          gradient: const LinearGradient(
                            begin: Alignment.centerLeft,
                            end: Alignment.centerRight,
                            colors: [
                              Color(0xffFF512F),
                              Color(0xffDD2475),
                            ],
                          ),
                        ),
                        child: Material(
                          borderRadius: BorderRadius.circular(50),
                          color: Colors.transparent,
                          // If user visits his own profile show edit, othervise show poke
                          child: InkWell(
                            borderRadius: BorderRadius.circular(50),
                            onTap: () => showModalBottomSheet(
                              context: context,
                              builder: (context) {
                                return _buildBottomSheet(
                                  user,
                                  session,
                                );
                              },
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(9),
                              child: Icon(
                                session.user['username'] == user['username']
                                    ? Icons.edit
                                    : Icons.star_outlined,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        user['username'],
                        style: const TextStyle(
                          fontSize: 26,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ],
                  ),
                  if (user['location'] != null)
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          user['location'],
                          style: const TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ],
                    ),
                  if (user['description'] != null)
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          user['description'],
                          style: const TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                            color: Colors.grey,
                          ),
                        ),
                      ],
                    ),
                ],
              ),
            ],
          ),
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
            padding: const EdgeInsets.symmetric(
              horizontal: 20,
              vertical: 20,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    const Icon(
                      Icons.adjust,
                      size: 28,
                      color: Color(0xFF444444),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: Text(
                        // TODO: Return count from the server, instead of this
                        (user["invites_sent"].length +
                                user["invites_received"].length)
                            .toString(),
                        textAlign: TextAlign.center,
                        style: const TextStyle(
                          color: Colors.grey,
                        ),
                      ),
                    ),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    const Icon(
                      Icons.date_range,
                      color: Color(0xFF444444),
                      size: 28,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: Text(
                        timeago.format(
                          DateTime.parse(user['created_at']),
                          locale: 'en_short',
                        ),
                        textAlign: TextAlign.center,
                        style: const TextStyle(
                          color: Colors.grey,
                        ),
                      ),
                    ),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: const [
                    Icon(
                      Icons.share_outlined,
                      size: 28,
                      color: Color(0xFF444444),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 10),
                      child: Text(
                        '20',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.grey,
                        ),
                      ),
                    ),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: const [
                    Icon(
                      Icons.archive_outlined,
                      color: Color(0xFF444444),
                      size: 28,
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 10),
                      child: Text(
                        '20',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.grey,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          const Divider(
            thickness: 1,
            indent: 40,
            endIndent: 40,
          ),
          Container(
            margin: const EdgeInsets.all(20),
            alignment: Alignment.centerLeft,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: const [
                Icon(
                  Icons.archive_outlined,
                  color: Colors.grey,
                ),
                SizedBox(
                  width: 10,
                ),
                Text(
                  'User posts',
                  style: TextStyle(
                    fontSize: 20,
                    letterSpacing: -1,
                  ),
                ),
              ],
            ),
          ),
          /*
          FutureBuilder(
            future: api.get('/posts', <String, String>{'userId': user['_id']}),
            builder: (
              context,
              AsyncSnapshot<http.Response> snapshot,
            ) {
              if (snapshot.hasData) {
                if (snapshot.data.statusCode == 200) {
                  final posts = jsonDecode(snapshot.data.body);

                  if (posts.length == 0) {
                    return Text(
                      'User hasn\'t posted anything.',
                    );
                  }

                  return Flexible(
                    child: ListView.builder(
                      itemCount: posts.length,
                      itemBuilder: (context, int index) {
                        return ListTile(
                          tileColor: Colors.grey,
                          leading: Container(
                            height: 40,
                            width: 40,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: Colors.grey[50],
                            ),
                            alignment: Alignment.center,
                            // Edit button on own profile, favorite button on guest's profile
                            child: session.user.isNotEmpty &&
                                    session.user['username'] == user['username']
                                ? IconButton(
                                    onPressed: () {},
                                    icon: Icon(Icons.edit),
                                    color: Color(0xff888888),
                                  )
                                : IconButton(
                                    onPressed: () {
                                      if (posts[index]['isFavorite'] == 0)
                                        _addFavorite(posts[index]['_id']);
                                      else
                                        _removeFavorite(posts[index]['_id']);
                                    },
                                    icon: posts[index]['isFavorite'] == 0
                                        ? Icon(Icons.favorite_border)
                                        : Icon(Icons.favorite),
                                    color: Color(0xffFF512F),
                                  ),
                          ),
                          title: Text(
                            posts[index]['content'],
                            style: TextStyle(
                              color: Color(0xff444444),
                              fontSize: 15,
                            ),
                          ),
                          subtitle: Text(
                            '2 days ago',
                            // posts[index]['createdAt'],
                            style: TextStyle(
                              color: Color(0xffFF512F),
                              fontSize: 11,
                            ),
                          ),
                        );
                      },
                    ),
                  );
                }
                return Container();
              } else
                return CircularProgressIndicator();
            },
          )
          */
        ],
      );
    });
  }

  Widget _buildBottomSheet(user, session) {
    if (session.user.isEmpty) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          ListTile(
            leading: const Icon(Icons.person_add_alt_1),
            title: const Text('Sign in'),
            onTap: () {
              Navigator.pushNamed(context, 'login');
            },
          ),
          ListTile(
            leading: const Icon(Icons.person_add_alt),
            title: const Text('Sign up'),
            onTap: () {
              Navigator.pushNamed(context, 'register');
            },
          ),
        ],
      );
    }
    if (user['username'] != session.user['username']) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          /*
          if (user['isFollowed'])
            ListTile(
              leading: Icon(Icons.remove_circle_rounded),
              title: Text('Unfollow'),
              onTap: () => _unfollow(user['_id']),
            ),
          if (!user['isFollowed'])
            ListTile(
              leading: Icon(Icons.add_circle_rounded),
              title: Text('Follow'),
              onTap: () => _follow(user['_id']),
            ),
          */
          ListTile(
            leading: const Icon(Icons.message_outlined),
            title: const Text('Invite'),
            onTap: () => _inviteUser(user['user_id']),
          ),
          ListTile(
            leading: const Icon(Icons.info),
            title: const Text('User information'),
            onTap: () {},
          ),
        ],
      );
    } else {
      return Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          ListTile(
            leading: const Icon(Icons.edit),
            title: const Text('Edit profile'),
            onTap: () => Navigator.pushNamed(
              context,
              'setup',
              arguments: {'user': user},
            ),
          ),
          ListTile(
            leading: const Icon(Icons.archive_outlined),
            title: const Text('Write a post'),
            onTap: () {
              // TODO: Add animation
              showGeneralDialog(
                context: context,
                pageBuilder: (
                  context,
                  Animation animation,
                  Animation secondaryAnimation,
                ) {
                  return Container(
                    decoration: const BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.centerLeft,
                        end: Alignment.centerRight,
                        colors: [
                          Color(0xffFF512F),
                          Color(0xffDD2475),
                        ],
                      ),
                    ),
                    child: Scaffold(
                      backgroundColor: Colors.transparent,
                      appBar: AppBar(
                        backgroundColor: Colors.transparent,
                        shadowColor: Colors.transparent,
                        title: const Text(
                          'Write a post',
                          style: TextStyle(
                            letterSpacing: -1,
                          ),
                        ),
                      ),
                      body: Container(
                        margin: const EdgeInsets.only(
                          top: 10,
                          left: 10,
                          right: 10,
                        ),
                        padding: const EdgeInsets.symmetric(
                          vertical: 20,
                          horizontal: 10,
                        ),
                        child: Form(
                          key: _postFormKey,
                          child: Column(
                            children: [
                              TextFormField(
                                controller: _postTextController,
                                validator: (value) {
                                  if (value.length > 100) {
                                    return 'The post contains more than 100 characters.';
                                  }

                                  return null;
                                },
                                minLines: 3,
                                maxLines: 4,
                                style: const TextStyle(
                                  color: Colors.white,
                                  fontSize: 18,
                                  letterSpacing: -1,
                                ),
                                decoration: const InputDecoration(
                                  hintText: 'What\'s on your mind',
                                  hintStyle: TextStyle(
                                    color: Colors.white,
                                  ),
                                  enabledBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Color(0xffFF512F),
                                      style: BorderStyle.solid,
                                    ),
                                  ),
                                  prefixIcon: Icon(
                                    Icons.edit,
                                    color: Colors.white,
                                  ),
                                  suffixText: '100',
                                ),
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  ElevatedButton(
                                    style: ButtonStyle(
                                      alignment: Alignment.center,
                                      shape: MaterialStateProperty.all<
                                          RoundedRectangleBorder>(
                                        RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(5),
                                        ),
                                      ),
                                      minimumSize: MaterialStateProperty.all(
                                        const Size(100, 30),
                                      ),
                                      backgroundColor:
                                          MaterialStateProperty.all(
                                        Colors.white,
                                      ),
                                    ),
                                    onPressed: () {
                                      if (_postFormKey.currentState
                                          .validate()) {
                                        _post();
                                      }
                                    },
                                    child: Row(
                                      mainAxisSize: MainAxisSize.min,
                                      children: const [
                                        Icon(
                                          Icons.check,
                                          color: Color(0xffFF512F),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(
                                            top: 5,
                                            bottom: 5,
                                          ),
                                          child: Text(
                                            'Post',
                                            style: TextStyle(
                                              color: Color(0xffFF512F),
                                              fontSize: 18,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  );
                },
              );
            },
          ),
          ListTile(
            leading: const Icon(Icons.copy),
            title: const Text('Copy invite link'),
            onTap: () {},
          ),
        ],
      );
    }
  }

  void _inviteUser(int userId) async {
    try {
      const storage = FlutterSecureStorage();

      final String apiKey =
          await storage.read(key: dotenv.env['BACKEND_TOKEN_KEY']);

      final http.Response response = await http.post(
        Uri.http(dotenv.env['API_URL'], '/invites/'),
        headers: {
          HttpHeaders.authorizationHeader: 'bearer $apiKey',
          HttpHeaders.contentTypeHeader: 'application/json; charset=UTF-8',
        },
        body: jsonEncode(
          <String, int>{'receiver_id': userId},
        ),
      );

      if (response.statusCode == 200) {
        Map data = jsonDecode(response.body);

        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return AlertDialog(
              title: const Text('Invite created'),
              content: const Text('Join the room immediately?'),
              actions: [
                TextButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (BuildContext context) => LobbyScreen(
                          roomId: data['invite_id'],
                        ),
                      ),
                    );
                  },
                  child: const Text('OK'),
                ),
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                    Navigator.of(context).pop();
                  },
                  child: const Text('Close'),
                ),
              ],
            );
          },
        );
      }
    } catch (err) {
      showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text('Oops, something wrong'),
            content: const Text('Error communicating with the server.'),
            actions: [
              TextButton(
                onPressed: () => Navigator.of(context).pop(),
                child: const Text('Cancel'),
              ),
            ],
          );
        },
      );
    }
  }

  /*
  void _addFavorite(String postId) async {
    // TODO: If user not logged in show options to login/register
    try {
      final response = await api.post(
        <String, dynamic>{'postId': postId},
        '/posts/favorite/',
      );

      if (response.statusCode == 200) {
        final data = jsonDecode(response.body);

        setState(() {});
        print(data);
      }
    } catch (err) {
      print('Error communicating with server: $err');
    }
  }

  void _removeFavorite(String postId) async {
    try {
      final response = await api.post(
        <String, String>{'postId': postId},
        '/posts/favorite/remove',
      );

      if (response.statusCode == 200) {
        final data = jsonDecode(response.body);

        setState(() {});
        print(data);
      }
    } catch (err) {
      print('Error communicating with server: $err');
    }
  }
  */

  void _post() async {
    try {
      const storage = FlutterSecureStorage();

      final String apiKey =
          await storage.read(key: dotenv.env['BACKEND_TOKEN_KEY']);

      final http.Response response = await http.post(
          Uri.http(dotenv.env['API_URL'], '/posts/'),
          headers: {HttpHeaders.authorizationHeader: 'bearer $apiKey'},
          body: <String, String>{'post': _postTextController.text});

      if (response.statusCode == 200) {
        // TODO: No server-side implementation
        _postTextController.clear();
      }

      // TODO: Handle error
    } catch (err) {
      developer.log(
        'Got error while posting',
        name: 'profile',
        error: err,
      );
    }
  }
}
