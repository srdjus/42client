import 'dart:convert';
import 'dart:io';
import 'dart:developer' as developer;
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart';
import 'package:lobby/components/setup/avatar_slide.dart';
import 'package:lobby/components/setup/description_slide.dart';
import 'package:lobby/components/setup/final_slide.dart';
import 'package:lobby/components/setup/gender_slide.dart';
import 'package:lobby/components/setup/location_slide.dart';
import 'package:http/http.dart' as http;
import 'package:lobby/models/session.dart';
import 'package:provider/provider.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

class SetupScreen extends StatefulWidget {
  const SetupScreen({Key key}) : super(key: key);

  @override
  State<SetupScreen> createState() => _SetupScreenState();
}

class _SetupScreenState extends State<SetupScreen> {
  bool _initialDataLoaded = false;

  final Map<String, String> _gender = {
    'value': 'Skipped',
    'initialValue': 'Skipped',
  };

  final Map<String, String> _description = {
    'value': '',
    'initialValue': '',
  };

  final Map<String, double> _age = {
    'value': 25.0,
    'initialValue': 25.0,
  };

  final Map<String, bool> _skipAge = {
    'value': false,
    'initialValue': false,
  };

  final Map<String, int> _avatar = {
    'value': 4,
    'initialValue': 4,
  };

  final Map<String, String> _location = {
    'value': '',
    'initialValue': '',
  };

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.centerLeft,
            end: Alignment.centerRight,
            colors: [
              Color(0xffFF512F),
              Color(0xffDD2475),
            ],
          ),
        ),
        child: FutureBuilder(
          future: _loadInitialData(),
          builder: (BuildContext context, AsyncSnapshot<Response> snapshot) {
            if (snapshot.hasData) {
              if (snapshot.data.statusCode == 200) {
                if (!_initialDataLoaded) {
                  // Initial data is populated once per request!
                  final initialData = jsonDecode(snapshot.data.body);

                  if (initialData['gender'] != null) {
                    _gender['value'] =
                        _gender['initialValue'] = initialData['gender'];
                  }

                  if (initialData['description'] != null) {
                    _description['value'] = _description['initialValue'] =
                        initialData['description'];
                  }

                  if (initialData['age'] != null) {
                    _age['value'] =
                        _age['initialValue'] = initialData['age'].toDouble();
                  }

                  if (initialData['ageHidden'] != null) {
                    _skipAge['value'] =
                        _skipAge['initialValue'] = initialData['ageHidden'];
                  }

                  if (initialData['location'] != null) {
                    _location['value'] =
                        _location['initialValue'] = initialData['location'];
                  }

                  if (initialData['avatar'] != null) {
                    _avatar['value'] =
                        _avatar['initialValue'] = initialData['avatar'];
                  }

                  _initialDataLoaded = true;
                }
                return PageView(
                  scrollDirection: Axis.vertical,
                  physics: const BouncingScrollPhysics(),
                  children: <Widget>[
                    GenderSlide(
                      currentGender: _gender['value'],
                      onPickedCallback: setGender,
                    ),
                    DescriptionSlide(
                      description: _description['value'],
                      age: _age['value'],
                      skipAge: _skipAge['value'],
                      onDescriptionChangedCallback: setDescription,
                      onAgePickedCallback: setAge,
                      onAgeSkippedCallback: setSkipAge,
                    ),
                    LocationSlide(
                      location: _location['value'],
                      onLocationChangedCallback: setLocation,
                    ),
                    AvatarSlide(
                      avatar: _avatar['value'],
                      onAvatarChangedCallback: setAvatar,
                    ),
                    FinalSlide(
                      onSubmittedCallback: _submitSetup,
                    ),
                  ],
                );
              }
              // TODO: Show setup loading error
              else {
                // Server returned an error status code
                return Container();
              }
            }

            if (snapshot.hasError) {
              return Container();
            }

            // Show loading
            return const Center(
              child: SizedBox(
                width: 60,
                height: 60,
                child: CircularProgressIndicator(
                  color: Colors.white,
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  // Setters
  void setGender(String gender) {
    setState(() => _gender['value'] = gender);
  }

  void setDescription(String description) {
    setState(() => _description['value'] = description);
  }

  void setAge(double age) {
    setState(() => _age['value'] = age);
  }

  void setSkipAge(bool value) {
    setState(() => _skipAge['value'] = value);
  }

  void setLocation(String location) {
    setState(() => _location['value'] = location);
  }

  void setAvatar(int avatar) {
    setState(() => _avatar['value'] = avatar);
  }

  Future<http.Response> _loadInitialData() async {
    const storage = FlutterSecureStorage();

    final apiKey = await storage.read(key: dotenv.env['BACKEND_TOKEN_KEY']);

    return http.get(
      Uri.http(dotenv.env['API_URL'], '/users/me/'),
      headers: {
        HttpHeaders.authorizationHeader: 'bearer $apiKey',
      },
    );
  }

  void _submitSetup() async {
    Map body = <String, dynamic>{
      if (_gender['value'] != _gender['initialValue'])
        'gender': _gender['value'],
      if (_description['value'] != _description['initialValue'])
        'description': _description['value'],
      if (_location['value'] != _location['initialValue'])
        'location': _location['value'],
      if (_avatar['value'] != _avatar['initialValue'])
        'avatar': _avatar['value'],
      if (_skipAge['value'] != _skipAge['initialValue'])
        'ageHidden': _skipAge['value'],
      if (!_skipAge['value'] && _age['value'] != _age['initialValue'])
        'age': _age['value']
    };

    final username = Provider.of<SessionModel>(
      context,
      listen: false,
    ).user['username'];

    if (body.isEmpty) {
      // Direct without sending a request
      Navigator.of(context).pushNamed(
        'profile',
        arguments: {
          'username': username,
        },
      );
    }

    try {
      const storage = FlutterSecureStorage();

      final apiKey = await storage.read(key: dotenv.env['BACKEND_TOKEN_KEY']);

      final http.Response response = await http.put(
        Uri.http(dotenv.env['API_URL'], '/users/me/'),
        headers: {
          HttpHeaders.authorizationHeader: 'bearer $apiKey',
          HttpHeaders.contentTypeHeader: 'application/json; charset=UTF-8',
        },
        body: jsonEncode(body),
      );

      if (response.statusCode == 200) {
        if (!mounted) return;

        Navigator.of(context).pushNamed(
          'profile',
          arguments: {
            'username': username,
          },
        );
      }
    } catch (err) {
      // TODO: Handle error - show error dialog
      developer.log(
        'Error updating profile',
        name: 'setup',
        error: err,
      );
    }
  }
}
