import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:developer' as developer;
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:lobby/components/curved_app_bar.dart';
import 'package:lobby/models/player.dart';
import 'package:lobby/screens/game.dart';
import 'package:web_socket_channel/io.dart';
import 'package:lobby/components/navigation_drawer.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

class LobbyScreen extends StatefulWidget {
  const LobbyScreen({Key key, @required this.roomId}) : super(key: key);

  final int roomId;

  @override
  State<LobbyScreen> createState() => _LobbyScreenState();
}

class _LobbyScreenState extends State<LobbyScreen>
    with TickerProviderStateMixin {
  String _status = 'Not connected';

  IOWebSocketChannel channel;
  StreamController wsStreamController;
  StreamSubscription wsStreamSubscription;

  AnimationController _animationController;
  Animation _colorTween;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const CurvedAppBar(
        title: 'Lobby',
      ),
      drawer: const NavigationDrawer(),
      body: Container(
        color: Colors.white,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: 100,
                width: 100,
                child: CircularProgressIndicator(
                  valueColor: _colorTween,
                  strokeWidth: 5.0,
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 20),
                child: Text(
                  _status,
                  style: const TextStyle(
                    color: Color(0xffDD2475),
                    fontSize: 25.0,
                    letterSpacing: -1.0,
                    fontWeight: FontWeight.w100,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _wsInit() async {
    const storage = FlutterSecureStorage();
    String apiKey = await storage.read(key: dotenv.env['BACKEND_TOKEN_KEY']);

    channel = IOWebSocketChannel.connect(
      'ws://${dotenv.env['API_URL']}/invites/${widget.roomId}/ws/',
      headers: <String, String>{
        HttpHeaders.cookieHeader: apiKey,
      },
    );

    // We need to share this in order to have multiple listeners
    wsStreamController = StreamController.broadcast();
    wsStreamController.addStream(channel.stream);

    wsStreamSubscription = wsStreamController.stream.listen(
      (event) {
        try {
          List eventData = jsonDecode(event);

          developer.log(eventData[0], name: "lobby");

          switch (eventData[0]) {
            case 'USER_JOINED':
              // Check response body
              if (eventData[1]['response'] == 'WAITING') {
                setState(() {
                  _status = 'Waiting for partner';
                });
              } else {
                // Extract users
                List<Player> players = eventData[1]['players']
                    .map((e) => Player.fromJson(e))
                    .toList()
                    .cast<Player>();

                // Cancel subscription and navigate after that
                wsStreamSubscription.pause();
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return GameScreen(
                        channel: channel,
                        wsStreamController: wsStreamController,
                        players: players,
                        playerIndex: eventData[1]['player_index'],
                      );
                    },
                  ),
                );
              }
              break;
            default:
              developer.log("Invalid event data", name: "lobby");
          }
        } catch (err) {
          // TODO: Handle error
          developer.log(
            "Invalid data - parsing",
            name: "lobby",
            error: err,
          );
        }
      },
    );
  }

  @override
  void initState() {
    _animationController =
        AnimationController(vsync: this, duration: const Duration(seconds: 5));

    _colorTween = _animationController.drive(
      ColorTween(
        begin: const Color(0xffFF512F),
        end: const Color(0xffDD2475),
      ),
    );

    _animationController.repeat(reverse: true);

    super.initState();

    _wsInit();
  }

  /* 
    TODO: Close socket connection when user leaves the lobby 
    Sink shouldn't be closed if user is redirected to game, only if user leaves (goes back)
  */
  @override
  void dispose() async {
    _animationController.dispose();
    super.dispose();

    await channel.sink.close();
    await wsStreamSubscription.cancel();
  }

  @override
  void deactivate() {
    super.deactivate();
  }
}
