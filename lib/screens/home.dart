import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:developer' as developer;
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:lobby/components/navigation_drawer.dart';
import 'package:lobby/models/session.dart';
import 'package:lobby/screens/lobby.dart';
import 'package:provider/provider.dart';
import 'package:web_socket_channel/io.dart';
import 'package:flutter_svg/flutter_svg.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen>
    with SingleTickerProviderStateMixin {
  bool isSearchOn = false;
  Timer timer;
  int currentTimerValue = 15;
  final _tagController = TextEditingController();

  // Animation (pulse/glow)
  AnimationController _animationController;
  Animation _animation;

  // Matchimaking
  bool isRoomCreated = false;
  int roomId = 0;

  IOWebSocketChannel channel;
  StreamController wsStreamController;
  StreamSubscription wsStreamSubscription;

  @override
  void initState() {
    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 2),
    );
    _animationController.repeat(reverse: true);
    _animation = Tween(begin: 0.0, end: 30.0).animate(_animationController)
      ..addListener(() {
        setState(() {});
      });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.centerLeft,
          end: Alignment.centerRight,
          colors: [
            Color(0xffFF512F),
            Color(0xffDD2475),
          ],
        ),
      ),
      child: Scaffold(
        extendBodyBehindAppBar: true,
        backgroundColor: Colors.transparent,
        drawer: const NavigationDrawer(),
        body: Consumer<SessionModel>(
          builder: (context, session, child) => Column(
            children: [
              const SizedBox(
                height: 80,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  IconButton(
                    onPressed: () => Scaffold.of(context).openDrawer(),
                    icon: const Icon(
                      Icons.menu,
                      color: Colors.white,
                    ),
                  ),
                  IconButton(
                    onPressed: () {
                      showDialog(
                        context: context,
                        builder: (context) {
                          return Dialog(
                            child: Container(
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(10),
                              ),
                              margin: const EdgeInsets.all(20),
                              padding: const EdgeInsets.all(30),
                              child: Column(
                                children: [
                                  Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: const [
                                      Icon(
                                        Icons.settings,
                                        color: Color(0xffDD2475),
                                      ),
                                      SizedBox(width: 5),
                                      Text(
                                        'Configure',
                                        style: TextStyle(
                                          fontSize: 20,
                                          color: Color(0xff666666),
                                          letterSpacing: -1,
                                        ),
                                      ),
                                    ],
                                  ),
                                  Expanded(
                                    child: TextField(
                                      controller: _tagController,
                                      decoration: const InputDecoration(
                                        labelText: 'Tag',
                                        hintText:
                                            'Something you like (e.g., cats, dogs, movies)',
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          );
                        },
                      );
                    },
                    icon: const Icon(Icons.settings),
                    color: const Color.fromRGBO(255, 255, 255, 1),
                  ),
                ],
              ),
              const SizedBox(
                height: 100,
              ),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    const Text(
                      'Randomize',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 25,
                          fontWeight: FontWeight.w600,
                          letterSpacing: -1,
                          shadows: [
                            Shadow(
                              color: Color.fromRGBO(0, 0, 0, 0.6),
                              offset: Offset(0.1, 0.1),
                              blurRadius: 10,
                            )
                          ]),
                    ),
                    SizedBox(
                      height: 400,
                      width: 300,
                      child: Center(
                        child: Container(
                          height: 200 + _animation.value,
                          width: 200 + _animation.value,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                blurRadius: _animation.value * 0.2,
                                spreadRadius: _animation.value * 0.2,
                                color: const Color.fromARGB(20, 0, 0, 0),
                              ),
                            ],
                          ),
                          child: InkWell(
                            highlightColor: Colors.pink,
                            splashColor: Colors.pink,
                            borderRadius: const BorderRadius.only(
                              topLeft: Radius.circular(60),
                              topRight: Radius.circular(70),
                              bottomLeft: Radius.circular(70),
                              bottomRight: Radius.circular(60),
                            ),
                            onTap: !isSearchOn ? _shuffle : _restartSearch,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                !isSearchOn
                                    ? SizedBox(
                                        height: 120 + _animation.value,
                                        width: 120 + _animation.value,
                                        child: SvgPicture.asset(
                                          'assets/images/logos/logo-transparent.svg',
                                          color: const Color.fromARGB(
                                              223, 35, 35, 35),
                                        ),
                                      )
                                    : Text(
                                        '$currentTimerValue',
                                        style: const TextStyle(
                                          color:
                                              Color.fromARGB(221, 246, 70, 70),
                                          fontSize: 20,
                                        ),
                                      ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _openSocketConnection() async {
    const storage = FlutterSecureStorage();

    final String apiKey =
        await storage.read(key: dotenv.env['BACKEND_TOKEN_KEY']);

    channel = IOWebSocketChannel.connect(
      'ws://${dotenv.env['API_URL']}/matchmaking/',
      headers: <String, String>{
        HttpHeaders.cookieHeader: apiKey,
        if (_tagController.text.isNotEmpty) 'tag': _tagController.text
      },
    );

    // We need to share this in order to have multiple listeners
    wsStreamController = StreamController.broadcast();
    wsStreamController.addStream(channel.stream);

    wsStreamSubscription = wsStreamController.stream.listen((event) {
      try {
        List eventData = jsonDecode(event);

        switch (eventData[0]) {
          case 'GAME_FOUND':
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (BuildContext context) => LobbyScreen(
                  roomId: eventData[1]['invite_id'],
                ),
              ),
            );

            setState(() {
              isSearchOn = false;
              isRoomCreated = true;
              roomId = eventData[1]['response'];
              currentTimerValue = 15;
            });

            break;

          case 'PARTNER_LEFT':
            _restartSearch();

            break;
        }
      } catch (err) {
        // Handle error
        developer.log(
          'Socket error',
          name: "home",
          error: err,
        );
      }
    });
  }

  void _closeSocketConnection() async {
    await channel.sink.close();
    await wsStreamSubscription.cancel();
  }

  void _shuffle() {
    /*
      Connection is kept open until user decides to stop the search.
      Search time is up to 15 seconds (reduced for dev purposes).
    */

    if (isRoomCreated) {
      // Just quit the current room
      _restartSearch();
      return;
    }

    _openSocketConnection();

    setState(() {
      isSearchOn = true;
    });

    int i = 0;
    // Checking with server if there is a partner

    timer = Timer.periodic(const Duration(seconds: 1), (t) {
      setState(() {
        currentTimerValue = 15 - ++i;
      });

      if (i > 15) _restartSearch();
    });
  }

  void _restartSearch() {
    timer.cancel();

    _closeSocketConnection();

    setState(() {
      isSearchOn = false;
      currentTimerValue = 15;
      isRoomCreated = false;
      roomId = 0;
    });
  }

  @override
  void dispose() {
    // TODO: Dispose for websocket?

    _animationController.dispose();

    super.dispose();
  }
}
