import 'package:flutter/material.dart';
import 'package:lobby/components/navigation_drawer.dart';
import 'package:lobby/localization/localization_const.dart';
import 'package:lobby/components/chat/category_selector.dart';

class MessagesScreen extends StatefulWidget {
  const MessagesScreen({Key key}) : super(key: key);

  @override
  State<MessagesScreen> createState() => _MessagesScreenState();
}

class _MessagesScreenState extends State<MessagesScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.centerLeft,
          end: Alignment.centerRight,
          colors: [
            Color(0xffFF512F),
            Color(0xffDD2475),
          ],
        ),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: PreferredSize(
          preferredSize: const Size.fromHeight(40),
          child: Container(
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.centerLeft,
                end: Alignment.centerRight,
                colors: [
                  Color(0xffFF512F),
                  Color(0xffDD2475),
                ],
              ),
            ),
            child: AppBar(
              backgroundColor: Colors.transparent,
              centerTitle: true,
              title: Text(
                getTranslated(context, 'chats'),
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 25,
                  letterSpacing: -0.5,
                ),
              ),
              elevation: 0,
              actions: <Widget>[
                IconButton(
                  icon: const Icon(Icons.search),
                  color: Colors.white,
                  iconSize: 24,
                  onPressed: () {},
                )
              ],
            ),
          ),
        ),
        drawer: const NavigationDrawer(),
        body: Column(
          children: const <Widget>[
            CategorySelector(),
          ],
        ),
      ),
    );
  }
}
