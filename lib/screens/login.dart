import 'dart:convert';
import 'dart:io';
import 'dart:developer' as developer;
import 'package:flutter_appauth/flutter_appauth.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:lobby/components/navigation_drawer.dart';
import 'package:lobby/localization/localization_const.dart';
import 'package:lobby/models/session.dart';
import 'package:provider/provider.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

import 'package:lobby/helpers/oauth2_service.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  Cookie sessionCookie;
  final _formKey = GlobalKey<FormState>();
  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();

  bool _obscureText = true;

  bool _loginError = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.centerLeft,
          end: Alignment.centerRight,
          colors: [
            Color(0xffFF512F),
            Color(0xffDD2475),
          ],
        ),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        drawer: const NavigationDrawer(),
        body: SingleChildScrollView(
          child: Container(
            padding: const EdgeInsets.all(20),
            child: Padding(
              padding: EdgeInsets.only(
                top: MediaQuery.of(context).size.height * 0.1,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const [
                      Image(
                        image: AssetImage('assets/images/dummy-logo.png'),
                        height: 70,
                        alignment: Alignment.center,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.07,
                  ),
                  Form(
                    key: _formKey,
                    child: Column(
                      children: [
                        TextFormField(
                          style: const TextStyle(color: Colors.white),
                          controller: _usernameController,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(7),
                              borderSide: BorderSide.none,
                            ),
                            focusedBorder: const OutlineInputBorder(
                              borderSide: BorderSide(
                                width: 1,
                                color: Color.fromRGBO(255, 255, 255, 0.6),
                              ),
                            ),
                            filled: true,
                            fillColor: const Color.fromRGBO(255, 255, 255, 0.3),
                            prefixIcon: const Icon(
                              Icons.person_rounded,
                              color: Colors.white,
                            ),
                            labelStyle: const TextStyle(
                              color: Colors.white,
                            ),
                            hintStyle: const TextStyle(
                              color: Colors.white,
                            ),
                            labelText: getTranslated(context, 'username'),
                            hintText: getTranslated(context, 'enter_username'),
                          ),
                        ),
                        const SizedBox(height: 25),
                        TextFormField(
                          style: const TextStyle(color: Colors.white),
                          controller: _passwordController,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(7),
                              borderSide: BorderSide.none,
                            ),
                            focusedBorder: const OutlineInputBorder(
                              borderSide: BorderSide(
                                width: 1,
                                color: Color.fromRGBO(
                                  255,
                                  255,
                                  255,
                                  0.6,
                                ),
                              ),
                            ),
                            filled: true,
                            fillColor: const Color.fromRGBO(255, 255, 255, 0.3),
                            prefixIcon: const Icon(
                              Icons.lock,
                              color: Colors.white,
                            ),
                            suffixIcon: GestureDetector(
                              onTap: () {
                                setState(() {
                                  _obscureText = !_obscureText;
                                });
                              },
                              child: Icon(
                                _obscureText
                                    ? Icons.visibility_off
                                    : Icons.visibility,
                                color: const Color.fromRGBO(255, 255, 255, 0.8),
                              ),
                            ),
                            labelStyle: const TextStyle(
                              color: Colors.white,
                            ),
                            hintStyle: const TextStyle(
                              color: Colors.white,
                            ),
                            labelText: getTranslated(context, 'password'),
                            hintText: getTranslated(context, 'enter_password'),
                          ),
                          obscureText: _obscureText,
                        ),
                        if (_loginError) ...[
                          const SizedBox(
                            height: 20,
                          ),
                          const Text(
                            'Check your credentials and try again.',
                            style: TextStyle(
                              fontSize: 16,
                              color: Colors.white,
                            ),
                          ),
                        ],
                        const SizedBox(
                          height: 20,
                        ),
                        ElevatedButton(
                          style: ButtonStyle(
                            shape: MaterialStateProperty.all<
                                RoundedRectangleBorder>(
                              RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30),
                              ),
                            ),
                            minimumSize: MaterialStateProperty.all(
                              const Size(200, 50),
                            ),
                            backgroundColor: MaterialStateProperty.all(
                              Colors.white,
                            ),
                          ),
                          onPressed: _submitForm,
                          child: Padding(
                            padding: const EdgeInsets.only(top: 10, bottom: 10),
                            child: Text(
                              getTranslated(context, "confirm"),
                              style: const TextStyle(
                                color: Color(0xffFF512F),
                                fontSize: 20,
                              ),
                            ),
                          ),
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Column(
                              children: [
                                Container(
                                  height: 3,
                                  width: 100,
                                  decoration: const BoxDecoration(
                                    gradient: LinearGradient(
                                      colors: [
                                        Color.fromRGBO(255, 255, 255, 0),
                                        Color.fromRGBO(255, 255, 255, 0.9),
                                      ],
                                      begin: Alignment.centerLeft,
                                      end: Alignment.centerRight,
                                    ),
                                  ),
                                )
                              ],
                            ),
                            Column(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(20),
                                  child: Text(
                                    getTranslated(context, 'or'),
                                    style: const TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.w600,
                                      fontSize: 18,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Column(
                              children: [
                                Container(
                                  height: 3,
                                  width: 100,
                                  decoration: const BoxDecoration(
                                    gradient: LinearGradient(
                                      colors: [
                                        Color.fromRGBO(255, 255, 255, 0.9),
                                        Color.fromRGBO(255, 255, 255, 0),
                                      ],
                                      begin: Alignment.centerLeft,
                                      end: Alignment.centerRight,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            TextButton.icon(
                              onPressed: _handleGoogleLogin,
                              icon: const FaIcon(
                                FontAwesomeIcons.google,
                                color: Colors.white,
                              ),
                              label: Padding(
                                padding: const EdgeInsets.only(
                                  left: 10,
                                ),
                                child: Text(
                                  getTranslated(context, 'google_signin'),
                                  style: const TextStyle(
                                      color: Colors.white,
                                      fontSize: 16,
                                      fontWeight: FontWeight.w200),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void _submitForm() async {
    // Submit native login form
    // TODO: Bug, when user enters incorrect credentials and then try to enter credentials again
    showDialog(
        // Loading, can't dismiss
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return Dialog(
            child: Container(
              padding: const EdgeInsets.all(10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: const [
                  Text(
                    'Checking credentials',
                    style: TextStyle(
                      fontSize: 18.0,
                      color: Colors.black54,
                      letterSpacing: -1,
                    ),
                  ),
                  CircularProgressIndicator(
                    color: Colors.pink,
                    strokeWidth: 2.0,
                  ),
                ],
              ),
            ),
          );
        });

    // Hide keyboard
    FocusScope.of(context).unfocus();

    try {
      final http.Response response = await http.post(
        Uri.http(dotenv.env['API_URL'], '/token/'),
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/x-www-form-urlencoded"
        },
        body: <String, String>{
          'username': _usernameController.text,
          'password': _passwordController.text,
        },
      );

      // Remove dialog
      Navigator.of(context).pop();

      if (response.statusCode == 200) {
        // Login successful
        final data = jsonDecode(response.body);

        // This one is used to update secure storage
        Map userData = data['user'];

        _handleLogin(userData, data['access_token']);
      } else if (response.statusCode == 400) {
        developer.log(
          'User not found, invaid credentials',
          name: 'login',
        );

        setState(() {
          _loginError = true;
        });
      }

      // TODO: Check for errors and loading
    } catch (err) {
      developer.log('Error communicating with server',
          name: 'login', error: err);
    }
  }

  void _handleLogin(Map userData, String accessToken) {
    // Pass data to Provider to update Session Model
    Provider.of<SessionModel>(context, listen: false)
        .login(userData, accessToken);

    Navigator.pushNamedAndRemoveUntil(
      context,
      'profile',
      (route) => false,
      arguments: {'username': userData['username']},
    );
  }

  void _handleGoogleLogin() async {
    final AuthorizationTokenResponse response =
        await OAuth2Service.instance.login();

    // Check if token is valid
    bool isValidResult = response != null &&
        response.accessToken != null &&
        response.idToken != null;

    if (!isValidResult || response.refreshToken == null) {
      // TODO: Handle error
      return;
    }

    try {
      final http.Response backendResponse = await http.post(
        Uri.http(dotenv.env['API_URL'], "/oauth2token/"),
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/x-www-form-urlencoded"
        },
        body: <String, String>{
          'token': response.idToken,
        },
        encoding: Encoding.getByName('utf-8'),
      );

      Map backendResponseData = jsonDecode(backendResponse.body);

      if (backendResponse.statusCode == 200) {
        const storage = FlutterSecureStorage();

        // Save refresh token
        await storage.write(
          key: dotenv.env['REFRESH_TOKEN_KEY'],
          value: response.refreshToken,
        );

        // Login successful, pass data to Provider to update Session Model
        _handleLogin(
          backendResponseData['user'],
          backendResponseData['access_token'],
        );
      } else if (backendResponse.statusCode == 400) {
        if (backendResponseData['detail'] == 'Native user exists') {
          showDialog(
            context: context,
            builder: (context) {
              return Dialog(
                child: Container(
                  padding: const EdgeInsets.all(10),
                  child: const Text(
                    'User with used e-mail already exists. However, you could still use '
                    'your e-mail and password to gain access to the application.',
                    style: TextStyle(
                      fontSize: 15.0,
                      color: Colors.black54,
                      letterSpacing: -1,
                    ),
                  ),
                ),
              );
            },
          );
        }
      }
    } catch (err) {
      developer.log(
        'Error communicating with a server',
        name: 'login',
        error: err,
      );
    }
  }
}
