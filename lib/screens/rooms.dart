import 'dart:convert';
import 'dart:io';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:lobby/components/curved_app_bar.dart';
import 'package:lobby/components/navigation_drawer.dart';
import 'package:lobby/localization/localization_const.dart';
import 'package:lobby/screens/lobby.dart';
import 'package:http/http.dart' as http;
import 'package:timeago/timeago.dart' as timeago;
import 'package:flutter_dotenv/flutter_dotenv.dart';

class RoomsScreen extends StatefulWidget {
  const RoomsScreen({Key key}) : super(key: key);

  @override
  State<RoomsScreen> createState() => _RoomsScreenState();
}

class _RoomsScreenState extends State<RoomsScreen> {
  Future<http.Response> _invitesResponse;
  bool _showHistory;

  @override
  void initState() {
    _showHistory = false;

    _invitesResponse = fetchInvites();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: CurvedAppBar(
        title: getTranslated(context, 'rooms'),
        icon: Icons.dashboard,
      ),
      drawer: const NavigationDrawer(),
      bottomNavigationBar: CurvedNavigationBar(
        onTap: (value) async {
          // value = 1 for history
          _invitesResponse = fetchInvites(isHistory: value != 0);

          setState(() {
            _showHistory = value == 1;
          });
        },
        height: 60,
        items: const [
          Icon(
            Icons.event_available_rounded,
            size: 27,
            color: Colors.white,
          ),
          Icon(
            Icons.history_rounded,
            size: 27,
            color: Colors.white,
          ),
        ],
        color: const Color(0xffEE3A52),
        backgroundColor: Colors.white,
        index: 0,
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(30),
        child: Container(
          decoration: const BoxDecoration(
            color: Colors.white,
          ),
          child: Column(
            children: [
              FutureBuilder(
                // TODO: Reload data on swipe down and after navigating to this route
                future: _invitesResponse,
                builder: (
                  BuildContext context,
                  AsyncSnapshot<http.Response> snapshot,
                ) {
                  List<Widget> children;

                  if (snapshot.connectionState == ConnectionState.done) {
                    if (snapshot.hasData && snapshot.data.statusCode == 200) {
                      if (_showHistory) {
                        children = _buildHistory(
                          jsonDecode(snapshot.data.body),
                        );
                      } else {
                        children = _buildList(
                          jsonDecode(snapshot.data.body),
                        );
                      }
                    } else if (snapshot.hasError) {
                      children = <Widget>[
                        Text(getTranslated(
                          context,
                          'have_error',
                        ))
                      ];
                    }
                  } else if (snapshot.connectionState ==
                      ConnectionState.waiting) {
                    children = <Widget>[
                      const SizedBox(
                        width: 60,
                        height: 60,
                        child: CircularProgressIndicator(),
                      ),
                      const Padding(
                        padding: EdgeInsets.only(top: 16),
                        child: Text('Loading...'),
                      )
                    ];
                  } else {
                    children = [];
                  }

                  return Column(
                    children: children,
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  List<Widget> _buildList(List invites) {
    List<Widget> children = [
      // List header
      const SizedBox(
        width: double.infinity,
        child: Text(
          'Active invites',
          textAlign: TextAlign.left,
          style: TextStyle(
            fontSize: 18,
            color: Color(0xffFF512F),
            letterSpacing: -0.5,
          ),
        ),
      ),
      const Divider(
        height: 1,
      ),
      const SizedBox(
        height: 20,
      ),
    ];

    final ListView roomList = ListView.builder(
      shrinkWrap: true,
      scrollDirection: Axis.vertical,
      itemCount: invites.length,
      reverse: true,
      physics: const NeverScrollableScrollPhysics(),
      itemBuilder: (
        BuildContext context,
        int index,
      ) {
        String prettyDate = timeago.format(
          DateTime.parse(invites[index]["created_at"]),
        );

        return ListTile(
          title: Text(
            '${invites[index]["sender"]["username"]} wants to connect with '
            '${invites[index]["receiver"]["username"]}',
            style: const TextStyle(
              fontSize: 13.0,
              letterSpacing: -0.5,
            ),
          ),
          subtitle: Text(prettyDate),
          leading: const CircleAvatar(
            backgroundImage: AssetImage('assets/images/avatars/6.png'),
            backgroundColor: Colors.amber,
          ),
          trailing: const Icon(Icons.sports_soccer),
          onTap: () => Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) =>
                  LobbyScreen(roomId: invites[index]['invite_id']),
            ),
          ),
        );
      },
    );

    children.add(roomList);

    if (invites.isEmpty) {
      children.add(const Text('Nothing to show.'));
    }

    return children;
  }

  List<Widget> _buildHistory(List invites) {
    List<Widget> children = [
      // List header
      const SizedBox(
        width: double.infinity,
        child: Text(
          'Invite history',
          textAlign: TextAlign.left,
          style: TextStyle(
            fontSize: 18,
            color: Color(0xffFF512F),
            letterSpacing: -0.5,
          ),
        ),
      ),
      const Divider(
        height: 1,
      ),
      const SizedBox(
        height: 20,
      ),
    ];

    final ListView roomList = ListView.builder(
      shrinkWrap: true,
      scrollDirection: Axis.vertical,
      physics: const NeverScrollableScrollPhysics(),
      itemCount: invites.length,
      reverse: true,
      itemBuilder: (BuildContext context, int index) {
        String prettyDate = timeago.format(DateTime.parse(
          invites[index]["created_at"],
        ));

        return ExpansionTile(
          collapsedTextColor: const Color(0xff555555),
          textColor: Colors.greenAccent,
          title: Text(
            '${invites[index]["sender"]["username"]} and ${invites[index]["receiver"]["username"]}',
            style: const TextStyle(
              fontSize: 15.0,
              letterSpacing: -0.5,
            ),
          ),
          subtitle: Text(
            prettyDate,
            style: const TextStyle(
              fontSize: 12.0,
              color: Color(0xff777777),
            ),
          ),
          leading: invites[index]['answers']?.length == 3
              ? const Icon(
                  Icons.check,
                  color: Colors.greenAccent,
                )
              : const Icon(
                  Icons.error,
                  color: Colors.redAccent,
                ),
          children: _buildAnswers(invites[index]['answers']),
        );
      },
    );

    children.add(roomList);

    if (invites.isEmpty) {
      children.add(
        const Text(
          'Nothing to show.',
        ),
      );
    }

    return children;
  }

  List<Widget> _buildAnswers(List answers) {
    if (answers == null) {
      return [];
    }

    List<Widget> children = [];

    for (int i = 0; i < answers.length; i++) {
      int choiceId = answers[i]['choice_id'];
      String questionBody = answers[i]['question']['body'];
      String questionCategory = answers[i]['question']['category'];
      String username = answers[i]['user']['username'];
      List choices = answers[i]['question']['choices'];

      // Selected choice
      dynamic choice =
          choices.firstWhere((element) => element['choice_id'] == choiceId);

      Widget data = ListTile(
        leading: Column(
          children: [
            const CircleAvatar(
              backgroundImage: AssetImage('assets/images/avatars/6.png'),
              backgroundColor: Colors.amber,
            ),
            Text(
              username,
              style: const TextStyle(fontSize: 9),
            ),
          ],
        ),
        title: Text(
          questionBody,
          style: const TextStyle(
            fontSize: 12.0,
          ),
        ),
        subtitle: Text(
          questionCategory,
          style: const TextStyle(
            fontSize: 11.0,
          ),
        ),
        trailing: Text(
          choice["body"],
          style: const TextStyle(
            fontSize: 13.0,
            color: Colors.green,
          ),
        ),
      );

      children.add(data);
    }

    return children;
  }

  Future<http.Response> fetchInvites({bool isHistory = false}) async {
    const storage = FlutterSecureStorage();

    final String apiKey =
        await storage.read(key: dotenv.env['BACKEND_TOKEN_KEY']);

    if (isHistory) {
      return http.get(
        Uri.http(dotenv.env['API_URL'], '/invites/history/'),
        headers: {HttpHeaders.authorizationHeader: 'bearer $apiKey'},
      );
    }

    return http.get(
      Uri.http(dotenv.env['API_URL'], '/invites/'),
      headers: {HttpHeaders.authorizationHeader: 'bearer $apiKey'},
    );
  }
}
