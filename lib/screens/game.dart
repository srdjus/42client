import 'dart:async';
import 'dart:convert';
import 'dart:developer' as developer;

import 'package:flutter/material.dart';
import 'package:lobby/components/game/answered_container.dart';
import 'package:lobby/components/game/game_over_container.dart';
import 'package:lobby/components/game/got_swipe_container.dart';
import 'package:lobby/components/game/not_ready_container.dart';
import 'package:lobby/components/game/partner_left_container.dart';
import 'package:lobby/components/game/pick_question_container.dart';
import 'package:lobby/components/game/picking_question_container.dart';
import 'package:lobby/components/game/question_container.dart';
import 'package:lobby/components/game/waiting_answer_container.dart';
import 'package:lobby/components/game/waiting_container.dart';
import 'package:lobby/components/game/waiting_question_container.dart';
import 'package:lobby/localization/localization_const.dart';
import 'package:lobby/models/game.dart';
import 'package:lobby/models/player.dart';
import 'package:lobby/models/question.dart';
import 'package:lobby/screens/chat.dart';
import 'package:lobby/types/game_status.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

class GameScreen extends StatefulWidget {
  const GameScreen({
    Key key,
    @required this.channel,
    @required this.wsStreamController,
    @required this.players,
    @required this.playerIndex,
  }) : super(key: key);

  final WebSocketChannel channel;
  final StreamController wsStreamController;
  final List<Player> players;
  final int playerIndex;

  @override
  State<GameScreen> createState() => _GameScreenState();
}

class _GameScreenState extends State<GameScreen> {
  Game game;

  // TODO: Remove this and reimplement through Game class
  List answersData = [];

  StreamSubscription wsStreamSubscription;

  @override
  void initState() {
    super.initState();

    // Initialize the game
    game = Game(widget.players);

    wsStreamSubscription = widget.wsStreamController.stream.listen(
      (event) {
        try {
          final List eventData = jsonDecode(event);

          developer.log(eventData[0], name: "game");

          switch (eventData[0]) {
            case 'GAME_READY':
              // Add questions to the game instance
              game.addQuestions(eventData[1]['availableQuestions']
                  .map((e) => Question.fromJson(e))
                  .toList()
                  .cast<Question>());

              if (game.currentPlayerIndex == widget.playerIndex) {
                setState(() {
                  game.status = GameStatus.pickingQuestion;
                });
              } else {
                setState(() {
                  game.status = GameStatus.waitingQuestion;
                });
              }

              break;

            case 'QUESTION_READY':
              setState(() {
                game.status = GameStatus.answeringQuestion;
                game.addPicked(eventData[1]['picked_index']);
              });

              break;

            case 'PARTNER_ANSWERED':
              setState(() {
                game.status = GameStatus.leavingFeedback;
                game.addAnswer(eventData[1]['choice_id']);
              });

              break;

            case 'NEW_QUESTIONS':
              // Change turns
              game.toggleCurrentPlayer();

              // Add questions to the game instance
              game.addQuestions(eventData[1]['availableQuestions']
                  .map((e) => Question.fromJson(e))
                  .toList()
                  .cast<Question>());

              if (game.currentPlayerIndex == widget.playerIndex) {
                setState(() {
                  game.status = GameStatus.pickingQuestion;
                });
              }

              break;

            case 'PARTNER_SWIPED':
              setState(() {
                game.status = GameStatus.gotSwipe;
                game.addSwipe(eventData[1]['swipeIndex']);
              });

              break;

            case 'PARTNER_LEFT':
              setState(() {
                game.status = GameStatus.partnerLeft;
              });

              break;

            case 'GAME_OVER':
              setState(() {
                game.status = GameStatus.gameOver;
                answersData = eventData[1]['answers'];
              });

              break;

            case 'CHAT_ACCEPTED':
              if (game.userWantsChat) {
                wsStreamSubscription.pause();

                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (BuildContext context) {
                      return ChatScreen(
                        channel: widget.channel,
                        wsStreamController: widget.wsStreamController,
                      );
                    },
                  ),
                );
              }

              setState(() {
                game.partnerWantsChat = true;
              });

              break;

            default:
              developer.log("Invalid event data", name: "game");
          }
        } catch (err) {
          // TODO: Handle error
          developer.log(
            "Invalid data - parsing",
            name: "game",
            error: err,
          );
        }
      },
    );
  }

  @override
  void dispose() async {
    super.dispose();

    await widget.channel.sink.close();
    await wsStreamSubscription.cancel();
  }

  @override
  void deactivate() {
    super.deactivate();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.centerLeft,
          end: Alignment.centerRight,
          colors: [
            Color(0xffFF512F),
            Color(0xffDD2475),
          ],
        ),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              if (game.status == GameStatus.notReady)
                NotReadyContainer(
                  readyUser: game.userReady,
                  sendReady: _sendReady,
                ),
              if (game.status == GameStatus.pickingQuestion)
                PickingQuestionContainer(
                  buildQuestions: _buildAvailableQuestions(),
                ),
              if (game.status == GameStatus.waitingQuestion)
                const WaitingQuestionContainer(),
              if (game.status == GameStatus.waitingAnswer)
                const WaitingAnswerContainer(),
              if (game.status == GameStatus.answeringQuestion)
                QuestionContainer(
                  body: game.currentQuestion.body,
                  choices: game.currentQuestion.choices,
                  onPickedCallback: _answerQuestion,
                  isAnswered:
                      game.status == GameStatus.waitingSwipe ? true : false,
                ),
              if (game.status == GameStatus.leavingFeedback)
                AnsweredContainer(
                  answer: game.currentAnswer,
                  onSwipeAnswer: _swipeAnswer,
                ),
              if (game.status == GameStatus.waitingSwipe)
                const WaitingContainer(),
              if (game.status == GameStatus.gotSwipe)
                GotSwipeContainer(
                  sendTurnOver: _sendTurnOver,
                  games: game.lastSwipe > 0
                      ? getTranslated(context, 'partner_likes')
                      : getTranslated(context, 'partner_dislikes'),
                ),
              if (game.status == GameStatus.partnerLeft)
                const PartnerLeftContainer(),
              if (game.status == GameStatus.gameOver)
                GameOverContainer(
                  buildResults: _buildResults(),
                  redirectToChat: _acceptChat,
                  partnerWantsChat: game.partnerWantsChat,
                ),
            ],
          ),
        ),
      ),
    );
  }

  // Build helpers
  List<PickQuestionContainer> _buildAvailableQuestions() {
    List<PickQuestionContainer> questions = List.empty(growable: true);

    for (int i = 0; i < game.availableQuestions.length; i++) {
      questions.add(
        // Single question item with corresponiding choices
        PickQuestionContainer(
            body: game.availableQuestions[i].body,
            choices: game.availableQuestions[i].choices,
            onPickedCallback: () => _pickQuestion(i)),
      );
    }
    return questions;
  }

  // Event handlers
  void _sendReady() {
    List message = [
      'NOTIFY_READY',
      {'data': true},
    ];

    // This should be validated on the server side
    setState(() {
      game.userReady = true;
    });

    widget.channel.sink.add(jsonEncode(message));
  }

  void _pickQuestion(int i) {
    // Index of question in a set (0-first, 1-second)
    List message = [
      'QUESTION_PICKED',
      {'index': i},
    ];

    setState(() {
      game.status = GameStatus.waitingAnswer;
      game.addPicked(i);
    });

    widget.channel.sink.add(jsonEncode(message));
  }

  void _answerQuestion(int choiceId) {
    List message = [
      'QUESTION_ANSWERED',
      {'choice_id': choiceId}
    ];

    setState(() {
      game.status = GameStatus.waitingSwipe;
      game.addAnswer(choiceId);
    });

    widget.channel.sink.add(jsonEncode(message));
  }

  void _swipeAnswer(int i) {
    List message = [
      'ANSWER_SWIPED',
      {'index': i}
    ];

    setState(() {
      game.status = GameStatus.waitingQuestion;
      game.addSwipe(i);
    });

    widget.channel.sink.add(jsonEncode(message));
  }

  void _sendTurnOver() {
    List message = ['TURN_OVER', {}];

    widget.channel.sink.add(jsonEncode(message));
  }

  List<Widget> _buildResults() {
    // TODO: Use ListBuilder and ListTile-s instead of this
    List<Widget> children = [];
    int upCount = 0;

    // Percentage
    for (int i = 0; i < answersData.length; i++) {
      if (answersData[i]['swipe'] == 1) upCount++;

      // Get question based on question_id
      Question question = game.questions
          .firstWhere((q) => q.questionId == answersData[i]['question_id']);

      // Find answer in list of choices for the question
      dynamic answer = question.choices
          .firstWhere((c) => c['choice_id'] == answersData[i]['choice_id']);

      // Build question list
      children.add(
        Container(
          padding: const EdgeInsets.all(20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(
                child: Text(
                  question.body,
                  style: const TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w200,
                    color: Colors.white,
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text(
                    answer['body'],
                    textAlign: TextAlign.right,
                    style: const TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  Icon(
                    answersData[i]['swipe'] > 0
                        ? Icons.star
                        : Icons.star_border_outlined,
                    color: Colors.white,
                  ),
                ],
              ),
            ],
          ),
        ),
      );
    }
    children.insert(
      0,
      Container(
        margin: const EdgeInsets.only(top: 20),
        child: Text(
          '${(upCount / 3.00 * 100).toStringAsFixed(0)}%',
          style: const TextStyle(
            fontSize: 50,
            color: Colors.white,
            fontWeight: FontWeight.bold,
            letterSpacing: -3,
          ),
        ),
      ),
    );

    return children;
  }

  void _acceptChat(BuildContext context) {
    List message = [
      'CHAT_ACCEPTED',
      {},
    ];

    widget.channel.sink.add(jsonEncode(message));

    if (game.partnerWantsChat) {
      wsStreamSubscription.pause();

      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (BuildContext context) {
            return ChatScreen(
              channel: widget.channel,
              wsStreamController: widget.wsStreamController,
            );
          },
        ),
      );

      return;
    }

    setState(() {
      game.userWantsChat = true;
    });
  }
}
