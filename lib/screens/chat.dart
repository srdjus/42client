import 'dart:async';
import 'dart:convert';
import 'dart:developer' as developer;

import 'package:flutter/material.dart';
import 'package:lobby/models/message.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

class ChatScreen extends StatefulWidget {
  const ChatScreen({
    Key key,
    @required this.channel,
    @required this.wsStreamController,
  }) : super(key: key);

  final WebSocketChannel channel;
  final StreamController wsStreamController;

  @override
  State<ChatScreen> createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  StreamSubscription wsStreamSubscription;
  final _messageController = TextEditingController();
  final _scrollController = ScrollController();
  final List<Message> _messages = List<Message>.empty(growable: true);

  @override
  void initState() {
    super.initState();

    wsStreamSubscription = widget.wsStreamController.stream.listen((event) {
      try {
        final List eventData = jsonDecode(event);

        switch (eventData[0]) {
          case 'GOT_MESSAGE':
            setState(() {
              _messages.add(
                Message(
                  time: '01:23',
                  text: eventData[1]["body"],
                  isLiked: false,
                  isMe: false,
                ),
              );
            });

            _scrollToBottom();

            break;
          default:
            break;
        }
      } catch (err) {
        developer.log(
          "Invalid data, can't be parsed.",
          name: "chat",
          error: err,
        );
      }
    });
  }

  @override
  void dispose() async {
    super.dispose();

    await wsStreamSubscription.cancel();
    await widget.channel.sink.close();
  }

  _buildMessage(Message message) {
    final Container msg = Container(
      width: MediaQuery.of(context).size.width * 0.75,
      margin: message.isMe
          ? const EdgeInsets.only(
              top: 8,
              bottom: 8,
              left: 80,
            )
          : const EdgeInsets.only(
              top: 8,
              bottom: 8,
            ),
      padding: const EdgeInsets.symmetric(
        horizontal: 25,
        vertical: 15,
      ),
      decoration: BoxDecoration(
        color: message.isMe ? Colors.yellow[50] : Colors.red[50],
        borderRadius: message.isMe
            ? const BorderRadius.only(
                topLeft: Radius.circular(15),
                bottomLeft: Radius.circular(15),
              )
            : const BorderRadius.only(
                topRight: Radius.circular(15),
                bottomRight: Radius.circular(15),
              ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            message.time,
            style: const TextStyle(
              color: Colors.grey,
              fontSize: 16,
              fontWeight: FontWeight.w600,
            ),
          ),
          const SizedBox(
            height: 8,
          ),
          Text(
            message.text,
            style: TextStyle(
              color: Colors.grey[800],
              fontSize: 16,
              fontWeight: FontWeight.w600,
            ),
          ),
        ],
      ),
    );
    if (message.isMe) {
      return msg;
    }
    return Row(
      children: [
        msg,
        IconButton(
          icon: message.isLiked
              ? const Icon(Icons.favorite)
              : const Icon(Icons.favorite_border),
          color: message.isLiked ? const Color(0xffFF512F) : Colors.grey,
          iconSize: 24,
          onPressed: () {},
        ),
      ],
    );
  }

  _buildMessageComposer() {
    return Container(
      padding: const EdgeInsets.symmetric(
        horizontal: 8,
      ),
      height: 70,
      color: Colors.white,
      child: Row(
        children: [
          IconButton(
            icon: const Icon(Icons.photo),
            onPressed: () {},
            iconSize: 25,
            color: Colors.grey,
          ),
          Expanded(
            child: TextField(
              textCapitalization: TextCapitalization.sentences,
              controller: _messageController,
              onChanged: (value) {},
              decoration: const InputDecoration.collapsed(
                hintText: 'Send a message...',
              ),
            ),
          ),
          IconButton(
            icon: const Icon(Icons.send),
            onPressed: _sendMessage,
            iconSize: 25,
            color: const Color(0xffDD2475),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(60),
        child: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
              colors: [
                Color(0xffFF512F),
                Color(0xffDD2475),
              ],
            ),
          ),
          child: AppBar(
            backgroundColor: Colors.transparent,
            title: const Text(
              'Partner',
              style: TextStyle(
                color: Colors.white,
                fontSize: 25,
                letterSpacing: -0.5,
              ),
            ),
            elevation: 0,
            actions: <Widget>[
              IconButton(
                icon: const Icon(Icons.more_horiz),
                color: Colors.white,
                iconSize: 24,
                onPressed: () {},
              ),
            ],
          ),
        ),
      ),
      body: Column(
        children: [
          Expanded(
            child: Container(
              decoration: const BoxDecoration(
                color: Colors.white,
              ),
              child: ListView.builder(
                reverse: false,
                padding: const EdgeInsets.only(
                  top: 35,
                ),
                itemCount: _messages.length,
                controller: _scrollController,
                itemBuilder: (BuildContext context, int index) {
                  return _buildMessage(_messages[index]);
                },
                physics: const BouncingScrollPhysics(), // Disables scroll glow
              ),
            ),
          ),
          _buildMessageComposer(),
        ],
      ),
    );
  }

  void _sendMessage() {
    if (_messageController.text.isEmpty) return;

    setState(() {
      _messages.add(Message(
        time: '01:23',
        text: _messageController.text,
        isLiked: false,
        isMe: true,
      ));
    });

    _scrollToBottom();

    widget.channel.sink.add(jsonEncode([
      'GOT_MESSAGE',
      {
        'body': _messageController.text,
      },
    ]));

    _messageController.clear();
  }

  void _scrollToBottom() {
    _scrollController.animateTo(
      // + 105 to pass the input height + padding
      _scrollController.position.maxScrollExtent + 105,
      duration: const Duration(seconds: 1),
      curve: Curves.fastOutSlowIn,
    );
  }
}
