import 'package:flutter/material.dart';
import 'package:lobby/classes/language.dart';
import 'package:lobby/localization/localization_const.dart';
import 'package:lobby/models/session.dart';
import 'package:provider/provider.dart';

import 'package:lobby/components/curved_app_bar.dart';
import 'package:lobby/components/navigation_drawer.dart';
import 'package:lobby/main.dart';

class SettingsScreen extends StatefulWidget {
  const SettingsScreen({Key key}) : super(key: key);

  @override
  State<SettingsScreen> createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  void _changeLanguage(Language language) async {
    Locale temp = await setLocale(language.languageCode);

    if (!mounted) return;

    MyApp.setLocale(context, temp);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: CurvedAppBar(
        title: getTranslated(context, 'settings'),
      ),
      drawer: const NavigationDrawer(),
      body: Container(
        margin: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.05),
        decoration: const BoxDecoration(
          color: Colors.white,
        ),
        child: ListView(
          children: [
            ListTile(
              leading: const Icon(Icons.language),
              title: Text(getTranslated(context, 'language')),
              trailing: DropdownButton(
                onChanged: (Language language) {
                  _changeLanguage(language);
                },
                icon: const Icon(Icons.keyboard_arrow_down_rounded),
                iconSize: 20,
                elevation: 16,
                style: const TextStyle(color: Colors.grey),
                underline: Container(
                  height: 0,
                ),
                items: Language.languageList()
                    .map<DropdownMenuItem<Language>>((lang) => DropdownMenuItem(
                          value: lang,
                          child: Row(
                            children: <Widget>[Text(lang.name)],
                          ),
                        ))
                    .toList(),
              ),
            ),
            ListTile(
              leading: const Icon(Icons.logout),
              title: const Text('Sign out'),
              onTap: () => showDialog(
                context: context,
                builder: (BuildContext context) => AlertDialog(
                  title: const Text("Warning!"),
                  content: const Text(
                    'You will be sign out. To confirm your decision click the confirm button.',
                  ),
                  actions: [
                    TextButton(
                      onPressed: () => Navigator.pop(context),
                      child: const Text(
                        'Dismiss',
                        style: TextStyle(
                          fontSize: 16,
                          color: Colors.grey,
                        ),
                      ),
                    ),
                    TextButton(
                      onPressed: _logout,
                      child: const Text(
                        'Confirm',
                        style: TextStyle(
                          fontSize: 16,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _logout() async {
    Provider.of<SessionModel>(
      context,
      listen: false,
    ).logout();
    Navigator.pushNamedAndRemoveUntil(
      context,
      'init',
      ((route) => false),
    );
  }
}
