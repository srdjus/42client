import 'package:flutter/cupertino.dart';
import 'package:lobby/localization/app_localization.dart';
import 'package:shared_preferences/shared_preferences.dart';

String getTranslated(BuildContext context, String key) {
  return AppLocalization.of(context).getTranslatedValue(key);
}

// language Code
const String eng = 'en';
const String srb = 'sr';
const String ger = 'de';

Future<Locale> setLocale(String languageCode) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();

  await prefs.setString("languageCode", languageCode);

  return _locale(languageCode);
}

Locale _locale(String languageCode) {
  Locale temp;
  switch (languageCode) {
    case eng:
      temp = Locale(languageCode, 'US');
      break;
    case srb:
      temp = Locale(languageCode, 'SP');
      break;
    case ger:
      temp = Locale(languageCode, 'DE');
      break;
    default:
      temp = const Locale(eng, 'US');
  }
  return temp;
}

Future<Locale> getLocale() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String languageCode = prefs.getString("languageCode") ?? eng;
  return _locale(languageCode);
}
