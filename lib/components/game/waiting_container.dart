import 'package:flutter/material.dart';

class WaitingContainer extends StatelessWidget {
  const WaitingContainer({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          colors: [
            Color(0xffFF512F),
            Color(0xffDD2475),
          ],
          begin: Alignment.centerLeft,
          end: Alignment.centerRight,
        ),
      ),
      child: const Center(
        child: Text(
          'Partner swiping',
          style: TextStyle(
            fontSize: 22,
            color: Colors.white,
            letterSpacing: -1.0,
            fontWeight: FontWeight.w200,
          ),
        ),
      ),
    );
  }
}
