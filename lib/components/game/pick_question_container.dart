import 'package:flutter/material.dart';

// Single (pickable) question with choices
class PickQuestionContainer extends StatefulWidget {
  const PickQuestionContainer({
    Key key,
    @required this.body,
    @required this.choices,
    @required this.onPickedCallback,
  }) : super(key: key);

  final String body;
  final List choices;
  final VoidCallback onPickedCallback;

  @override
  State<PickQuestionContainer> createState() => _PickQuestionContainerState();
}

class _PickQuestionContainerState extends State<PickQuestionContainer> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        vertical: 0,
        horizontal: 20,
      ),
      child: Column(
        children: [
          SizedBox(
            width: double.infinity,
            child: ElevatedButton(
              style: ButtonStyle(
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5),
                  ),
                ),
                backgroundColor: MaterialStateProperty.all(Colors.white),
              ),
              onPressed: widget.onPickedCallback,
              child: Padding(
                padding: const EdgeInsets.only(top: 15, bottom: 15),
                child: Text(
                  widget.body,
                  style: const TextStyle(
                    fontSize: 16,
                    color: Color(0xffFF512F),
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
              top: 20,
              bottom: 40,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                for (Map choice in widget.choices)
                  Text(
                    '${choice["body"]}',
                    style: const TextStyle(
                      fontSize: 17,
                      color: Colors.white,
                    ),
                  ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
