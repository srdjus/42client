import 'package:flutter/material.dart';

// Single question with it's "pickable" choices
class QuestionContainer extends StatefulWidget {
  const QuestionContainer({
    Key key,
    @required this.body,
    @required this.choices,
    @required this.onPickedCallback,
    @required this.isAnswered,
  }) : super(key: key);

  final String body;
  final List choices;
  final ValueSetter<int> onPickedCallback;
  final bool isAnswered;

  @override
  State<QuestionContainer> createState() => _QuestionContainerState();
}

class _QuestionContainerState extends State<QuestionContainer> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.centerLeft,
          end: Alignment.centerRight,
          colors: [
            Color(0xffFF512F),
            Color(0xffDD2475),
          ],
        ),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            margin: const EdgeInsets.only(
              bottom: 20,
            ),
            child: const Text(
              "We got a question",
              style: TextStyle(
                fontSize: 22,
                color: Colors.white,
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.all(20),
            margin: const EdgeInsets.all(10),
            child: Padding(
              padding: const EdgeInsets.all(15),
              child: Column(
                children: [
                  Text(
                    widget.body,
                    textAlign: TextAlign.center,
                    style: const TextStyle(
                      color: Colors.white,
                      fontSize: 22,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              ..._buildAnswerTiles(),
            ],
          ),
        ],
      ),
    );
  }

  List<Widget> _buildAnswerTiles() {
    List<Widget> buttons = List.empty(growable: true);

    for (int i = 0; i < widget.choices.length; i++) {
      buttons.add(
        Expanded(
          child: Padding(
            padding: const EdgeInsets.all(10),
            child: ElevatedButton(
              style: ButtonStyle(
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5),
                  ),
                ),
                backgroundColor: MaterialStateProperty.all(Colors.white),
              ),
              onPressed: () => widget.isAnswered
                  ? null
                  : widget.onPickedCallback(widget.choices[i]['choice_id']),
              child: Padding(
                padding: const EdgeInsets.only(top: 10, bottom: 10),
                child: Text(
                  '${widget.choices[i]["body"]}',
                  style: const TextStyle(
                    fontSize: 18,
                    color: Color(0xffFF512F),
                  ),
                ),
              ),
            ),
          ),
        ),
      );
    }
    return buttons;
  }
}
