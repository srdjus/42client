import 'package:flutter/material.dart';

class PickingQuestionContainer extends StatelessWidget {
  const PickingQuestionContainer({
    Key key,
    @required this.buildQuestions,
  }) : super(key: key);

  final List buildQuestions;
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        height: MediaQuery.of(context).size.height,
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.centerLeft,
            end: Alignment.centerRight,
            colors: [
              Color(0xffFF512F),
              Color(0xffDD2475),
            ],
          ),
        ),
        child: Padding(
          padding: EdgeInsets.only(
            top: MediaQuery.of(context).size.height * 0.15,
          ),
          child: Column(
            children: [
              const Padding(
                padding: EdgeInsets.only(
                  bottom: 40,
                ),
                child: Text(
                  'Pick one of the questions for your partner',
                  style: TextStyle(
                    fontSize: 18,
                    color: Colors.white,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
              ...buildQuestions,
            ],
          ),
        ),
      ),
    );
  }
}
