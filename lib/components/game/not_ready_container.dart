import 'package:flutter/material.dart';

class NotReadyContainer extends StatelessWidget {
  const NotReadyContainer({
    Key key,
    @required this.readyUser,
    @required this.sendReady,
  }) : super(key: key);

  final bool readyUser;
  final Function sendReady;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ElevatedButton(
          onPressed: readyUser ? null : sendReady,
          style: ButtonStyle(
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30),
              ),
            ),
            minimumSize: MaterialStateProperty.all(
              const Size(200, 50),
            ),
            backgroundColor: MaterialStateProperty.all(
              Colors.white,
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.only(top: 10, bottom: 10),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: const [
                Text(
                  'Ready',
                  style: TextStyle(
                    color: Color(0xffFF512F),
                    fontSize: 20,
                  ),
                ),
                Icon(
                  Icons.arrow_forward,
                  color: Colors.red,
                )
              ],
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(top: 20),
          child: const Text(
            'Waiting for both users to confirm.',
            style: TextStyle(
              fontSize: 20,
              color: Colors.white,
              fontWeight: FontWeight.w100,
              letterSpacing: -1.0,
            ),
          ),
        ),
      ],
    );
  }
}
