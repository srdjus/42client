import 'package:flutter/material.dart';

class PartnerLeftContainer extends StatelessWidget {
  const PartnerLeftContainer({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          colors: [
            Color(0xffFF512F),
            Color(0xffDD2475),
          ],
          begin: Alignment.centerLeft,
          end: Alignment.centerRight,
        ),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Padding(
            padding: EdgeInsets.only(
              bottom: 20,
            ),
            child: Text(
              'Lost the connection to the partner',
              style: TextStyle(fontSize: 20, color: Colors.white),
            ),
          ),
          ElevatedButton(
            style: ButtonStyle(
              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30),
                ),
              ),
              minimumSize: MaterialStateProperty.all(
                const Size(200, 50),
              ),
              backgroundColor: MaterialStateProperty.all(Colors.white),
            ),
            onPressed: () => Navigator.pushNamed(context, 'rooms'),
            child: const Padding(
              padding: EdgeInsets.only(top: 10, bottom: 10),
              child: Text(
                "Proceed",
                style: TextStyle(
                  color: Color(0xffFF512F),
                  fontSize: 20,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
