import 'package:flutter/material.dart';

class GotSwipeContainer extends StatefulWidget {
  const GotSwipeContainer({
    Key key,
    @required this.sendTurnOver,
    @required this.games,
  }) : super(key: key);

  final Function sendTurnOver;
  final String games;

  @override
  State<GotSwipeContainer> createState() => _GotSwipeContainerState();
}

class _GotSwipeContainerState extends State<GotSwipeContainer> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          widget.games,
          style: const TextStyle(
            fontSize: 22,
            color: Colors.white,
            letterSpacing: -1.0,
            fontWeight: FontWeight.w200,
          ),
        ),
        const SizedBox(
          height: 20,
        ),
        ElevatedButton(
          onPressed: widget.sendTurnOver,
          style: ButtonStyle(
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30),
              ),
            ),
            minimumSize: MaterialStateProperty.all(
              const Size(200, 50),
            ),
            backgroundColor: MaterialStateProperty.all(
              Colors.white,
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.only(top: 10, bottom: 10),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: const [
                Text(
                  'Next one',
                  style: TextStyle(
                    color: Color(0xffFF512F),
                    fontSize: 20,
                  ),
                ),
                Icon(
                  Icons.arrow_forward,
                  color: Colors.red,
                )
              ],
            ),
          ),
        ),
      ],
    );
  }
}
