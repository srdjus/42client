import 'package:flutter/material.dart';

class AnsweredContainer extends StatefulWidget {
  const AnsweredContainer({
    Key key,
    @required this.answer,
    @required this.onSwipeAnswer,
  }) : super(key: key);

  final Map answer;
  final ValueSetter<int> onSwipeAnswer;

  @override
  State<AnsweredContainer> createState() => _AnsweredContainerState();
}

class _AnsweredContainerState extends State<AnsweredContainer> {
  double swipeSign = 0; // -1 for dislike, 1 for like

  List<Color> buttonColors() {
    if (swipeSign == 0) {
      return const [
        Color(0xffFFFFFF),
        Color(0xffEEEEEE),
      ];
    } else if (swipeSign == 1) {
      return const [
        Color.fromARGB(255, 184, 230, 138),
        Color.fromARGB(255, 158, 244, 205)
      ];
    } else {
      return const [
        Color.fromARGB(255, 251, 161, 161),
        Color.fromARGB(255, 253, 161, 183),
      ];
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const Text(
          'Partner says',
          style: TextStyle(
            letterSpacing: -1,
            fontSize: 25,
            color: Colors.white,
            fontWeight: FontWeight.w100,
          ),
        ),
        Dismissible(
          onUpdate: (DismissUpdateDetails details) {
            if (details.progress > 0) {
              if (details.direction == DismissDirection.startToEnd) {
                setState(() {
                  swipeSign = 1;
                });
              } else if (details.direction == DismissDirection.endToStart) {
                setState(() {
                  swipeSign = -1;
                });
              }
            } else {
              setState(() {
                swipeSign = 0;
              });
            }
          },
          key: const Key('0'),
          background: const Icon(
            Icons.thumb_up_alt_outlined,
            color: Color.fromARGB(255, 192, 243, 217),
            size: 40,
          ),
          secondaryBackground: const Icon(
            Icons.thumb_down_alt_outlined,
            color: Color.fromARGB(255, 255, 193, 193),
            size: 40,
          ),
          child: AnimatedContainer(
            duration: const Duration(milliseconds: 400),
            curve: Curves.easeIn,
            margin: const EdgeInsets.only(top: 40),
            padding: const EdgeInsets.all(60),
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: buttonColors(),
              ),
            ),
            child: Text(
              '${widget.answer["body"]}',
              style: TextStyle(
                color: swipeSign == 0 ? const Color(0xffDD2475) : Colors.white,
                fontSize: 20,
              ),
            ),
          ),
          onDismissed: (direction) {
            direction == DismissDirection.startToEnd
                ? widget.onSwipeAnswer(1)
                : widget.onSwipeAnswer(-1);
          },
        ),
      ],
    );
  }
}
