import 'package:flutter/material.dart';
import 'package:lobby/localization/localization_const.dart';
import 'package:lobby/models/session.dart';
import 'package:provider/provider.dart';

@immutable
class ClipShadowPath extends StatelessWidget {
  final Shadow shadow;
  final CustomClipper<Path> clipper;
  final Widget child;

  const ClipShadowPath({
    Key key,
    @required this.shadow,
    @required this.clipper,
    @required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomPaint(
      key: UniqueKey(),
      painter: _ClipShadowShadowPainter(
        clipper: clipper,
        shadow: shadow,
      ),
      child: ClipPath(clipper: clipper, child: child),
    );
  }
}

class _ClipShadowShadowPainter extends CustomPainter {
  final Shadow shadow;
  final CustomClipper<Path> clipper;

  _ClipShadowShadowPainter({@required this.shadow, @required this.clipper});

  @override
  void paint(Canvas canvas, Size size) {
    var paint = shadow.toPaint();
    var clipPath = clipper.getClip(size).shift(shadow.offset);
    canvas.drawPath(clipPath, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}

class TriangleClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final path = Path();
    path.lineTo(0, 0.0);
    path.lineTo(size.width / 2, size.height);
    path.lineTo(size.width, size.height);
    path.lineTo(size.width, 0.0);
    path.lineTo(size.width, 0.0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(TriangleClipper oldClipper) => false;
}

class NavigationDrawer extends StatelessWidget {
  const NavigationDrawer({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Consumer<SessionModel>(
        builder: (context, session, child) => ListView(
          padding: const EdgeInsets.all(0),
          children: [
            Stack(
              children: [
                SizedBox(
                  width: MediaQuery.of(context).size.width,
                  child: DrawerHeader(
                    padding: const EdgeInsets.only(top: 20),
                    decoration: const BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.centerLeft,
                        end: Alignment.centerRight,
                        colors: [
                          Color(0xffFF512F),
                          Color(0xffDD2475),
                        ],
                      ),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(
                        left: 15,
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            width: 70,
                            height: 70,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              border: Border.all(
                                color: const Color(0xffFF512F),
                                width: 2,
                              ),
                              image: const DecorationImage(
                                fit: BoxFit.fill,
                                image: AssetImage('assets/images/man.png'),
                              ),
                            ),
                            child: CircleAvatar(
                              backgroundImage: AssetImage(session
                                      .user.isNotEmpty
                                  ? 'assets/images/avatars/${session.user["avatar"]}.png'
                                  : 'assets/images/man.png'),
                            ),
                          ),
                          Text(
                            getTranslated(context, 'welcome'),
                            style: const TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                              letterSpacing: -1,
                            ),
                          ),
                          Text(
                            session.user.isNotEmpty
                                ? session.user['username']
                                : getTranslated(context, 'guest'),
                            style: const TextStyle(
                              color: Colors.white,
                              fontSize: 15,
                              letterSpacing: -0.5,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                Positioned(
                  top: 40,
                  right: 0,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left: 15),
                            child: SizedBox(
                              height: 70,
                              width: MediaQuery.of(context).size.width * 0.28,
                              child: Image.asset(
                                'assets/images/dummy-logo.png',
                                alignment: const Alignment(1, -0.55),
                                fit: BoxFit.contain,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
            ListTile(
              leading: Icon(
                Icons.home,
                size: 30,
                color: Colors.grey[700],
              ),
              title: Text(
                getTranslated(context, 'home'),
                style: TextStyle(
                  color: Colors.grey[600],
                  fontSize: 17,
                  letterSpacing: -0.5,
                ),
              ),
              onTap: () => Navigator.pushNamed(context, 'home'),
            ),
            if (session.user.isEmpty)
              ListTile(
                leading: Icon(
                  Icons.person_add_alt_1,
                  size: 30,
                  color: Colors.grey[700],
                ),
                title: Text(
                  getTranslated(context, 'login'),
                  style: TextStyle(
                    color: Colors.grey[600],
                    fontSize: 17,
                    letterSpacing: -0.5,
                  ),
                ),
                onTap: () => Navigator.pushNamed(context, 'login'),
              ),
            if (session.user.isEmpty)
              ListTile(
                leading: Icon(
                  Icons.person_add_alt,
                  size: 30,
                  color: Colors.grey[700],
                ),
                title: Text(
                  getTranslated(context, 'register'),
                  style: TextStyle(
                    color: Colors.grey[600],
                    fontSize: 17,
                    letterSpacing: -0.5,
                  ),
                ),
                onTap: () => Navigator.pushNamed(context, 'register'),
              ),
            if (session.user.isNotEmpty)
              ListTile(
                leading: Icon(
                  Icons.person,
                  size: 30,
                  color: Colors.grey[700],
                ),
                title: Text(
                  getTranslated(context, 'profile'),
                  style: TextStyle(
                    color: Colors.grey[600],
                    fontSize: 17,
                    letterSpacing: -0.5,
                  ),
                ),
                onTap: () => Navigator.pushNamed(
                  context,
                  'profile',
                  arguments: {'username': session.user['username']},
                ),
              ),
            if (session.user.isNotEmpty)
              ListTile(
                leading: Icon(
                  Icons.search,
                  size: 30,
                  color: Colors.grey[700],
                ),
                title: Text(
                  getTranslated(context, 'rooms'),
                  style: TextStyle(
                    color: Colors.grey[600],
                    fontSize: 17,
                    letterSpacing: -0.5,
                  ),
                ),
                onTap: () => Navigator.pushNamed(context, 'rooms'),
              ),
            if (session.user.isNotEmpty)
              ListTile(
                leading: Icon(
                  Icons.settings,
                  size: 30,
                  color: Colors.grey[700],
                ),
                title: Text(
                  getTranslated(context, 'settings'),
                  style: TextStyle(
                    color: Colors.grey[600],
                    fontSize: 17,
                    letterSpacing: -0.5,
                  ),
                ),
                onTap: () => Navigator.pushNamed(context, 'settings'),
              ),
          ],
        ),
      ),
    );
  }
}
