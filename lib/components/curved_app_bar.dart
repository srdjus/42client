import 'package:flutter/material.dart';

class CustomClipPath extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();

    path.lineTo(0, size.height);

    // Y growing by 0.2 and X growing by 0.1
    path.cubicTo(
      0,
      size.height * 0.8,
      size.width * 0.1,
      size.height * 0.7,
      size.width * 0.2,
      size.height * 0.7,
    );

    path.lineTo(
      size.width * 0.8,
      size.height * 0.7,
    );

    path.cubicTo(
      size.width * 0.9,
      size.height * 0.7,
      size.width,
      size.height * 0.8,
      size.width,
      size.height,
    );

    path.lineTo(size.width, 0);

    path.close();

    return path;
  }

  @override
  shouldReclip(CustomClipPath oldClipper) => false;
}

class CurvedAppBar extends StatefulWidget implements PreferredSizeWidget {
  const CurvedAppBar({
    Key key,
    @required this.title,
    this.icon,
    this.showDrawer = true,
    this.showSearch = true,
  })  : preferredSize = const Size.fromHeight(140),
        super(key: key);

  final String title;
  final IconData icon;
  final bool showDrawer;
  final bool showSearch;

  @override
  final Size preferredSize;

  @override
  State<CurvedAppBar> createState() => _CurvedAppBarState();
}

class _CurvedAppBarState extends State<CurvedAppBar> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 400,
      color: Colors.white,
      child: ClipPath(
        clipper: CustomClipPath(),
        child: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.centerLeft,
              end: Alignment.center,
              colors: [
                Color(0xffFF512F),
                Color(0xffDD2475),
              ],
            ),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              if (widget.showDrawer)
                IconButton(
                  icon: const Icon(Icons.menu),
                  color: Colors.white,
                  onPressed: () => Scaffold.of(context).openDrawer(),
                ),
              Row(
                children: [
                  if (widget.icon != null)
                    Icon(
                      widget.icon,
                      color: Colors.white,
                      size: 28,
                    ),
                  Padding(
                    padding: const EdgeInsets.only(
                      left: 5,
                      right: 5,
                    ),
                    child: Text(
                      widget.title,
                      style: const TextStyle(
                        color: Colors.white,
                        fontSize: 28,
                        fontWeight: FontWeight.w100,
                        letterSpacing: -0.5,
                      ),
                    ),
                  ),
                ],
              ),
              if (widget.showSearch)
                IconButton(
                  icon: const Icon(Icons.search),
                  color: Colors.white,
                  onPressed: () => Navigator.pushNamed(context, 'search'),
                ),
            ],
          ),
        ),
      ),
    );
  }
}
