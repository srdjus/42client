import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:lobby/localization/localization_const.dart';

class GenderSlide extends StatefulWidget {
  const GenderSlide({
    Key key,
    @required this.currentGender,
    @required this.onPickedCallback,
  }) : super(key: key);

  final String currentGender;
  final ValueSetter<String> onPickedCallback;

  @override
  State<GenderSlide> createState() => _GenderSlideState();
}

class _GenderSlideState extends State<GenderSlide>
    with TickerProviderStateMixin {
  AnimationController _maleAnimationController;
  AnimationController _femaleAnimationController;
  Animation _maleColorTween;
  Animation _femaleColorTween;

  @override
  void initState() {
    _maleAnimationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 300),
    );

    _femaleAnimationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 300),
    );

    _maleColorTween = ColorTween(
      begin: Colors.transparent,
      end: Colors.white,
    ).animate(_maleAnimationController);

    _femaleColorTween = ColorTween(
      begin: Colors.transparent,
      end: Colors.white,
    ).animate(_femaleAnimationController);

    // Initial animation
    if (widget.currentGender == 'male') {
      _maleAnimationController.forward();
    }

    if (widget.currentGender == 'female') {
      _femaleAnimationController.forward();
    }

    super.initState();
  }

  @override
  void dispose() {
    _maleAnimationController.dispose();
    _femaleAnimationController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.centerLeft,
          end: Alignment.centerRight,
          colors: [
            Color(0xffFF512F),
            Color(0xffDD2475),
          ],
        ),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            getTranslated(context, 'welcome_lacios'),
            style: const TextStyle(
              fontSize: 26,
              color: Colors.white,
              fontWeight: FontWeight.w600,
            ),
          ),
          Text(
            getTranslated(context, 'introduce'),
            style: const TextStyle(
              color: Colors.white,
              fontSize: 16,
            ),
          ),
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.1,
          ),
          GestureDetector(
            onTap: () {
              if (widget.currentGender != 'male') {
                _maleAnimationController.forward();
                _femaleAnimationController.reverse();
              }

              widget.onPickedCallback('male');
            },
            child: AnimatedBuilder(
              animation: _maleColorTween,
              builder: (context, child) => Container(
                height: 66,
                width: 66,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(100),
                  color: _maleColorTween.value,
                  border: widget.currentGender == 'male'
                      ? Border.all(
                          width: 2,
                          color: Colors.transparent,
                        )
                      : Border.all(
                          width: 2,
                          color: Colors.white,
                        ),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    FaIcon(
                      FontAwesomeIcons.mars,
                      color: widget.currentGender == 'male'
                          ? const Color(0xffEE3A52)
                          : Colors.white,
                      size: 30,
                    ),
                  ],
                ),
              ),
            ),
          ),
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.05,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Column(
                children: [
                  Container(
                    height: 3,
                    width: 100,
                    decoration: const BoxDecoration(
                      gradient: LinearGradient(
                        colors: [
                          Color.fromRGBO(255, 255, 255, 0),
                          Color.fromRGBO(255, 255, 255, 0.9),
                        ],
                        begin: Alignment.centerLeft,
                        end: Alignment.centerRight,
                      ),
                    ),
                  )
                ],
              ),
              Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(20),
                    child: Text(
                      getTranslated(context, 'or'),
                      style: const TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w600,
                        fontSize: 18,
                      ),
                    ),
                  ),
                ],
              ),
              Column(
                children: [
                  Container(
                    height: 3,
                    width: 100,
                    decoration: const BoxDecoration(
                      gradient: LinearGradient(
                        colors: [
                          Color.fromRGBO(255, 255, 255, 0.9),
                          Color.fromRGBO(255, 255, 255, 0),
                        ],
                        begin: Alignment.centerLeft,
                        end: Alignment.centerRight,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.05,
          ),
          GestureDetector(
            onTap: () {
              if (widget.currentGender != 'female') {
                _femaleAnimationController.forward();
                _maleAnimationController.reverse();
              }

              widget.onPickedCallback('female');
            },
            child: AnimatedBuilder(
              animation: _femaleColorTween,
              builder: (context, child) => Container(
                height: 66,
                width: 66,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(100),
                  color: _femaleColorTween.value,
                  border: widget.currentGender == 'female'
                      ? Border.all(
                          width: 2,
                          color: Colors.transparent,
                        )
                      : Border.all(
                          width: 2,
                          color: Colors.white,
                        ),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    FaIcon(
                      FontAwesomeIcons.venus,
                      color: widget.currentGender == 'female'
                          ? const Color(0xffEE3A52)
                          : Colors.white,
                      size: 30,
                    ),
                  ],
                ),
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(
              top: 30,
            ),
            child: InkWell(
              onTap: () {
                _femaleAnimationController.reverse();
                _maleAnimationController.reverse();

                widget.onPickedCallback('Skipped');
              },
              child: Text(
                getTranslated(context, 'rather_not_say'),
                style: const TextStyle(
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
