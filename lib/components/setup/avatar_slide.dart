import 'package:flutter/material.dart';

class AvatarSlide extends StatefulWidget {
  const AvatarSlide({
    Key key,
    @required this.avatar,
    @required this.onAvatarChangedCallback,
  }) : super(key: key);

  final int avatar;
  final ValueSetter<int> onAvatarChangedCallback;

  @override
  State<AvatarSlide> createState() => _AvatarSlideState();
}

class _AvatarSlideState extends State<AvatarSlide> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        height: MediaQuery.of(context).size.height,
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.centerLeft,
            end: Alignment.centerRight,
            colors: [
              Color(0xffFF512F),
              Color(0xffDD2475),
            ],
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CircleAvatar(
              backgroundImage: AssetImage(
                'assets/images/avatars/${widget.avatar}.png',
              ),
              radius: 50.0,
            ),
            Container(
              margin: const EdgeInsets.symmetric(vertical: 10.0),
              child: const Text(
                'Pick the avatar that suits your personality',
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.4,
              width: MediaQuery.of(context).size.width * 0.8,
              child: Container(
                padding: const EdgeInsets.all(20.0),
                decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    boxShadow: [
                      BoxShadow(
                        color: Color.fromRGBO(25, 25, 25, 0.1),
                        spreadRadius: 5,
                        blurRadius: 5,
                        offset: Offset(2, 2),
                      )
                    ]),
                child: SingleChildScrollView(
                  child: Wrap(
                    spacing: 20.0,
                    runSpacing: 20.0,
                    alignment: WrapAlignment.center,
                    crossAxisAlignment: WrapCrossAlignment.center,
                    children: [..._loadAvatars()],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  List<Widget> _loadAvatars() {
    List<Widget> children = [];

    for (int i = 1; i <= 51; i++) {
      // Hardcoded for now!
      children.add(
        InkWell(
          onTap: () {
            widget.onAvatarChangedCallback(i);
          },
          child: CircleAvatar(
            backgroundImage: AssetImage('assets/images/avatars/$i.png'),
            radius: 30.0,
          ),
        ),
      );
    }

    return children;
  }
}
