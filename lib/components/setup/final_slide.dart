import 'package:flutter/material.dart';
import 'package:lobby/localization/localization_const.dart';

class FinalSlide extends StatefulWidget {
  const FinalSlide({
    Key key,
    @required this.onSubmittedCallback,
  }) : super(key: key);

  final VoidCallback onSubmittedCallback;

  @override
  State<FinalSlide> createState() => _FinalSlideState();
}

class _FinalSlideState extends State<FinalSlide> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.centerLeft,
          end: Alignment.centerRight,
          colors: [
            Color(0xffFF512F),
            Color(0xffDD2475),
          ],
        ),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            getTranslated(context, 'well_done'),
            style: const TextStyle(
              fontSize: 35,
              color: Colors.white,
              fontWeight: FontWeight.w400,
            ),
          ),
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.05,
          ),
          Image(
            width: MediaQuery.of(context).size.width * 0.5,
            image: const AssetImage(
              'assets/images/setup-finished.png',
            ),
          ),
          Text(
            getTranslated(context, 'ready_to_go'),
            textAlign: TextAlign.center,
            style: const TextStyle(
              color: Colors.white,
            ),
          ),
          const SizedBox(
            height: 30,
          ),
          ElevatedButton(
            style: ButtonStyle(
              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30),
                ),
              ),
              minimumSize: MaterialStateProperty.all(
                const Size(200, 50),
              ),
              backgroundColor: MaterialStateProperty.all(Colors.white),
            ),
            onPressed: widget.onSubmittedCallback,
            child: const Padding(
              padding: EdgeInsets.only(top: 10, bottom: 10),
              child: Text(
                "Finish",
                style: TextStyle(
                  color: Color(0xffFF512F),
                  fontSize: 20,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
