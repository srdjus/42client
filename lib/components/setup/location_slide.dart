import 'package:flutter/material.dart';
import 'package:lobby/localization/localization_const.dart';

class LocationSlide extends StatefulWidget {
  const LocationSlide({
    Key key,
    @required this.location,
    @required this.onLocationChangedCallback,
  }) : super(key: key);

  final String location;
  final ValueSetter<String> onLocationChangedCallback;

  @override
  State<LocationSlide> createState() => _LocationSlideState();
}

class _LocationSlideState extends State<LocationSlide> {
  final _locationController = TextEditingController();

  @override
  void initState() {
    _locationController.text = widget.location;

    _locationController.addListener(() {
      widget.onLocationChangedCallback(_locationController.text);
    });

    super.initState();
  }

  @override
  void dispose() {
    _locationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        height: MediaQuery.of(context).size.height,
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.centerLeft,
            end: Alignment.centerRight,
            colors: [
              Color(0xffFF512F),
              Color(0xffDD2475),
            ],
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              getTranslated(context, 'where_are_you'),
              style: const TextStyle(
                color: Colors.white,
                fontSize: 19,
              ),
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.07,
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.8,
              child: TextFormField(
                controller: _locationController,
                textAlign: TextAlign.center,
                decoration: InputDecoration(
                  enabledBorder: const UnderlineInputBorder(
                      borderSide: BorderSide(
                    width: 3,
                    color: Colors.white,
                  )),
                  focusedBorder: const UnderlineInputBorder(
                      borderSide: BorderSide(
                    width: 3,
                    color: Colors.white,
                  )),
                  hintText: getTranslated(context, 'street_city_country'),
                  hintStyle: const TextStyle(
                    color: Color.fromRGBO(255, 255, 255, 0.8),
                  ),
                ),
                style: const TextStyle(
                  fontSize: 18,
                  fontFamily: 'OpenSans',
                  color: Colors.white,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
