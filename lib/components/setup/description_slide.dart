import 'package:flutter/material.dart';
import 'package:lobby/localization/localization_const.dart';

class DescriptionSlide extends StatefulWidget {
  const DescriptionSlide({
    Key key,
    @required this.description,
    @required this.age,
    @required this.skipAge,
    @required this.onDescriptionChangedCallback,
    @required this.onAgePickedCallback,
    @required this.onAgeSkippedCallback,
  }) : super(key: key);

  final String description;
  final double age;
  final bool skipAge;
  final ValueSetter<String> onDescriptionChangedCallback;
  final ValueSetter<double> onAgePickedCallback;
  final ValueSetter<bool> onAgeSkippedCallback;

  @override
  State<DescriptionSlide> createState() => _DescriptionSlideState();
}

class _DescriptionSlideState extends State<DescriptionSlide> {
  final _descriptionController = TextEditingController();

  @override
  void initState() {
    _descriptionController.text = widget.description;

    _descriptionController.addListener(() {
      widget.onDescriptionChangedCallback(_descriptionController.text);
    });

    super.initState();
  }

  @override
  void dispose() {
    _descriptionController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        height: MediaQuery.of(context).size.height,
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.centerLeft,
            end: Alignment.centerRight,
            colors: [
              Color(0xffFF512F),
              Color(0xffDD2475),
            ],
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              getTranslated(context, 'describe_yourself'),
              style: const TextStyle(
                color: Colors.white,
                fontSize: 19,
              ),
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.07,
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.8,
              child: TextFormField(
                controller: _descriptionController,
                textAlign: TextAlign.center,
                decoration: InputDecoration(
                  enabledBorder: const UnderlineInputBorder(
                      borderSide: BorderSide(
                    width: 3,
                    color: Colors.white,
                  )),
                  focusedBorder: const UnderlineInputBorder(
                      borderSide: BorderSide(
                    width: 3,
                    color: Colors.white,
                  )),
                  hintText: getTranslated(context, 'your_description'),
                  hintStyle: const TextStyle(
                    color: Color.fromRGBO(255, 255, 255, 0.8),
                  ),
                ),
                style: const TextStyle(
                  fontSize: 18,
                  fontFamily: 'OpenSans',
                  color: Colors.white,
                ),
              ),
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.1,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Column(
                  children: [
                    Container(
                      height: 3,
                      width: 100,
                      decoration: const BoxDecoration(
                        gradient: LinearGradient(
                          colors: [
                            Color.fromRGBO(255, 255, 255, 0),
                            Color.fromRGBO(255, 255, 255, 0.9),
                          ],
                          begin: Alignment.centerLeft,
                          end: Alignment.centerRight,
                        ),
                      ),
                    )
                  ],
                ),
                Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(20),
                      child: Text(
                        getTranslated(context, 'and'),
                        style: const TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                          fontSize: 18,
                        ),
                      ),
                    ),
                  ],
                ),
                Column(
                  children: [
                    Container(
                      height: 3,
                      width: 100,
                      decoration: const BoxDecoration(
                        gradient: LinearGradient(
                          colors: [
                            Color.fromRGBO(255, 255, 255, 0.9),
                            Color.fromRGBO(255, 255, 255, 0),
                          ],
                          begin: Alignment.centerLeft,
                          end: Alignment.centerRight,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.05,
            ),
            Text(
              getTranslated(context, 'how_old'),
              style: const TextStyle(
                color: Colors.white,
                fontSize: 19,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                top: 50,
              ),
              child: SliderTheme(
                data: SliderTheme.of(context).copyWith(
                  activeTrackColor: Colors.white,
                  inactiveTrackColor: const Color.fromRGBO(255, 255, 255, 0.3),
                  valueIndicatorShape: const PaddleSliderValueIndicatorShape(),
                  valueIndicatorColor: const Color(0xffEE3A52),
                  valueIndicatorTextStyle: const TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                  ),
                  thumbColor: Colors.white,
                  thumbShape:
                      const RoundSliderThumbShape(enabledThumbRadius: 12.0),
                ),
                child: Slider(
                  value: widget.age,
                  min: 18,
                  max: 99,
                  divisions: 81,
                  label: widget.age.round().toString(),
                  onChanged: widget.skipAge
                      ? null
                      : (double value) {
                          widget.onAgePickedCallback(value);
                        },
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  getTranslated(context, 'skip_age'),
                  style: const TextStyle(
                    color: Colors.white,
                  ),
                ),
                Switch(
                  value: widget.skipAge,
                  onChanged: (value) => widget.onAgeSkippedCallback(value),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
