import 'package:flutter/material.dart';
import 'package:lobby/localization/localization_const.dart';

class InterestsSlide extends StatefulWidget {
  const InterestsSlide({
    Key key,
    @required this.initialInterests,
    @required this.onInterestsChangedCallback,
  }) : super(key: key);

  final List<String> initialInterests;
  final ValueSetter<List<String>> onInterestsChangedCallback;

  @override
  State<InterestsSlide> createState() => _InterestsSlideState();
}

class _InterestsSlideState extends State<InterestsSlide> {
  final List<Map> _interests = [
    {
      'name': 'Sports',
      'isChecked': false,
    },
    {
      'name': 'Music',
      'isChecked': false,
    },
    {
      'name': 'Film',
      'isChecked': false,
    },
    {
      'name': 'Games',
      'isChecked': false,
    },
    {
      'name': 'Religion',
      'isChecked': false,
    },
    {
      'name': 'Politics',
      'isChecked': false,
    },
    {
      'name': 'Economy',
      'isChecked': false,
    },
    {
      'name': 'Love',
      'isChecked': false,
    },
  ];

  @override
  void initState() {
    setState(() {
      for (var interest in _interests) {
        if (widget.initialInterests.contains(interest['name'])) {
          interest['isChecked'] = true;
        }
      }
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        height: MediaQuery.of(context).size.height,
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.centerLeft,
            end: Alignment.centerRight,
            colors: [
              Color(0xffFF512F),
              Color(0xffDD2475),
            ],
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              getTranslated(context, 'your_interests'),
              style: const TextStyle(
                color: Colors.white,
                fontSize: 16,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                top: 30,
              ),
              child: SingleChildScrollView(
                child: SizedBox(
                  height: MediaQuery.of(context).size.height * 0.6,
                  child: ListView(
                    children: _interests
                        .map(
                          (entry) => CheckboxListTile(
                            checkColor: Colors.white,
                            title: Text(
                              entry['name'],
                              style: const TextStyle(
                                color: Colors.white,
                              ),
                            ),
                            value: entry['isChecked'],
                            onChanged: (value) {
                              setState(() {
                                entry['isChecked'] = value;
                              });

                              widget.onInterestsChangedCallback(
                                  filterCheckedInterests());
                            },
                          ),
                        )
                        .toList(),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  List<String> filterCheckedInterests() {
    return _interests
        .where((element) => element['isChecked'])
        .map((element) => element['name'])
        .toList()
        .cast<String>();
  }
}
