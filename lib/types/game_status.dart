enum GameStatus {
  notReady,
  pickingQuestion,
  waitingQuestion,
  waitingAnswer,
  answeringQuestion,
  waitingSwipe,
  leavingFeedback,
  gotSwipe,
  partnerLeft,
  gameOver,
}
