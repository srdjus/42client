import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:lobby/localization/app_localization.dart';
import 'package:lobby/localization/localization_const.dart';
import 'package:lobby/screens/home.dart';
import 'package:lobby/screens/init.dart';
import 'package:lobby/screens/messages.dart';
import 'package:lobby/screens/rooms.dart';
import 'package:flutter/material.dart';
import 'package:lobby/screens/settings.dart';
import 'package:lobby/screens/setup.dart';
import 'package:provider/provider.dart';
import 'package:lobby/models/session.dart';
import 'package:lobby/screens/login.dart';
import 'package:lobby/screens/profile.dart';
import 'package:lobby/screens/register.dart';
import 'package:lobby/screens/search.dart';

void main() async {
  await dotenv.load(fileName: ".env");
  runApp(
    ChangeNotifierProvider(
      create: (context) => SessionModel(),
      child: const MyApp(),
    ),
  );
}

class MyApp extends StatefulWidget {
  const MyApp({Key key}) : super(key: key);

  static void setLocale(BuildContext context, Locale locale) {
    _MyAppState state = context.findAncestorStateOfType<_MyAppState>();
    state.setLocale(locale);
  }

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Locale _locale;

  void setLocale(Locale locale) {
    setState(() {
      _locale = locale;
    });
  }

  @override
  void didChangeDependencies() {
    getLocale().then((locale) {
      setState(() {
        _locale = locale;
      });
    });

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    if (_locale == null) {
      return const Center(
        child: CircularProgressIndicator(
          backgroundColor: Colors.white,
        ),
      );
    } else {
      return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Lacios',
        theme: ThemeData(
          backgroundColor: Colors.white,
          fontFamily: 'OpenSans',
          primarySwatch: Colors.pink,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        locale: _locale,
        supportedLocales: const [
          Locale('en', 'US'),
          Locale('sr', 'SP'),
          Locale('de', 'DE'),
        ],
        localizationsDelegates: const [
          AppLocalization.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        localeResolutionCallback: (deviceLocale, supportedLocals) {
          for (var locale in supportedLocals) {
            if (locale.languageCode == deviceLocale.languageCode &&
                locale.countryCode == deviceLocale.countryCode) {
              return deviceLocale;
            }
          }
          return supportedLocals.first;
        },
        initialRoute: 'init',
        routes: {
          'init': (context) => const InitScreen(),
          'profile': (context) => const ProfileScreen(),
          'login': (context) => const LoginScreen(),
          'register': (context) => const RegisterScreen(),
          'home': (context) => const HomeScreen(),
          'rooms': (context) => const RoomsScreen(),
          'setup': (context) => const SetupScreen(),
          'settings': (context) => const SettingsScreen(),
          'messages': (context) => const MessagesScreen(),
          'search': (context) => const SearchScreen(),
        },
      );
    }
  }
}
