class Message {
  // Would usually be type DateTime or Firebase Timestamp in production apps
  final String time;
  final String text;
  final bool isLiked;
  final bool isMe;

  Message({
    this.time,
    this.text,
    this.isLiked,
    this.isMe,
  });
}
