import 'package:lobby/models/player.dart';
import 'package:lobby/models/question.dart';
import 'package:lobby/types/game_status.dart';

/// Defines a complete game instance.
///
/// [_questions] contains all the questions including ones that are not chosen
/// and [_picked], [_answers], [_swipes] define indexes for the chosen question,
/// id of specific choices and swipes.
/// [status] is a enum type, keeping the game status
class Game {
  final int nQuestions;

  int _currentPlayerIndex = 0;
  GameStatus status = GameStatus.notReady;

  // For now without function just for styles
  bool userReady = false;

  bool userWantsChat = false;
  bool partnerWantsChat = false;

  // This one is placeholder for game stats
  final List<Player> _players; // TODO: Implement through this
  final List<Question> _questions = [];
  final List<int> _picked = [];
  final List<int> _answers = [];
  final List<int> _swipes = [];

  Game(this._players, {this.nQuestions = 2});

  int get currentPlayerIndex => _currentPlayerIndex;

  Question get currentQuestion {
    int currentPickedIndex = _picked[_picked.length - 1];

    return _questions[_questions.length - 2 + currentPickedIndex];
  }

  int get lastSwipe => _swipes[_swipes.length - 1];

  Map get currentAnswer {
    // Get choiceId of last answer
    int currentAnswerChoiceId = _answers[_answers.length - 1];

    // Index of last question in last set 0 if first question is picked, 1 if second
    int currentPickedIndex = _picked[_picked.length - 1];

    // Last/current questionj
    Question currentQuestion =
        _questions[_questions.length - 2 + currentPickedIndex];

    // Find answer based on choiceId
    for (int i = 0; i < currentQuestion.choices.length; i++) {
      if (currentQuestion.choices[i]['choice_id'] == currentAnswerChoiceId) {
        return currentQuestion.choices[i];
      }
    }

    return null;
  }

  List<Question> get availableQuestions {
    return <Question>[
      _questions[_questions.length - 2],
      _questions[_questions.length - 1],
    ];
  }

  List<Question> get questions {
    return _questions;
  }

  void addQuestions(List<Question> questions) => _questions.addAll(questions);
  void addPicked(int picked) => _picked.add(picked);
  void addAnswer(int answer) => _answers.add(answer);
  void addSwipe(int swipe) => _swipes.add(swipe);
  void toggleCurrentPlayer() {
    _currentPlayerIndex = _currentPlayerIndex == 1 ? 0 : 1;
  }
}
