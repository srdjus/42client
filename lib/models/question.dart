class Question {
  int questionId;
  String body;
  List choices; // Create a Choice class
  String category;

  Question(
    this.questionId,
    this.body,
    this.choices,
    this.category,
  );

  Question.fromJson(Map json)
      : questionId = json['question_id'],
        body = json['body'],
        choices = json['choices'],
        category = json['category'];
}
