import 'dart:convert';
import 'dart:developer' as developer;
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;

class SessionModel extends ChangeNotifier {
  Map _user = {};
  Map get user => _user;

  void login(Map u, String accessToken) async {
    _user = u;

    const storage = FlutterSecureStorage();

    // Login successful
    await storage.write(
      key: dotenv.env['BACKEND_TOKEN_KEY'],
      value: accessToken,
    );

    notifyListeners();
  }

  void logout() async {
    _user.clear();

    const storage = FlutterSecureStorage();

    storage.delete(key: dotenv.env['BACKEND_TOKEN_KEY']);

    notifyListeners();
  }

  Future<Map> checkSession() async {
    // Before session check, check session type
    const storage = FlutterSecureStorage();

    final String backendToken =
        await storage.read(key: dotenv.env['BACKEND_TOKEN_KEY']);

    if (backendToken == null) {
      // No user.
      return {};
    }

    try {
      final http.Response response = await http.get(
          Uri.http(dotenv.env['API_URL'], '/users/me'),
          headers: <String, String>{
            HttpHeaders.authorizationHeader: 'bearer $backendToken',
            HttpHeaders.contentTypeHeader: 'application/json; charset=UTF-8',
            HttpHeaders.acceptHeader: 'application/json'
          });

      if (response.statusCode == 200) {
        _user = jsonDecode(response.body);
      } else {
        _user = {};
        developer.log('No session', name: 'info');
      }

      return _user;
    } catch (err) {
      developer.log(
        'Error communicating with a server',
        name: 'session',
        error: err,
      );

      return {};
    }
  }
}
