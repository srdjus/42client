import 'package:flutter/services.dart';
import 'package:flutter_appauth/flutter_appauth.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'dart:developer' as developer;
import 'dart:io' show Platform;

class OAuth2Service {
  // Singleton class (we need only one instance)
  static final OAuth2Service instance = OAuth2Service._();

  factory OAuth2Service() {
    return instance;
  }

  OAuth2Service._();

  final FlutterAppAuth _appAuth = const FlutterAppAuth();
  final FlutterSecureStorage _secureStorage = const FlutterSecureStorage();

  final String _clientId = Platform.isAndroid
      ? dotenv.env['CLIENT_ID_ANDROID']
      : dotenv.env['CLIENT_ID_IOS'];

  final String _redirectUrl = Platform.isAndroid
      ? dotenv.env['REDIRECT_URL_ANDROID']
      : dotenv.env['REDIRECT_URL_IOS'];

  final String _issuer = dotenv.env['GOOGLE_ISSUER'];

  Future<TokenResponse> initAuth() async {
    final storedRefreshToken = await _secureStorage.read(
      key: dotenv.env['REFRESH_TOKEN_KEY'],
    );

    if (storedRefreshToken == null) {
      // No OAuth2 session
      return null;
    }

    try {
      return await _appAuth.token(
        TokenRequest(
          _clientId,
          _redirectUrl,
          issuer: _issuer,
          refreshToken: storedRefreshToken,
        ),
      );
    } catch (err) {
      developer.log(
        'Got error in OAuth initial authentication',
        name: 'oauth',
        error: err,
      );
      return null;
    }
  }

  Future<AuthorizationTokenResponse> login() async {
    try {
      final authorizationTokenRequest = AuthorizationTokenRequest(
        _clientId,
        _redirectUrl,
        issuer: _issuer,
        scopes: ['email', 'profile'],
      );

      return await _appAuth.authorizeAndExchangeCode(authorizationTokenRequest);
    } on PlatformException {
      developer.log(
        'Got error in OAuth login, Platform Exception',
        name: 'oauth',
      );
      return null;
    } catch (err) {
      developer.log(
        'Got error in OAuth login',
        name: 'oauth',
      );
      return null;
    }
  }
}
