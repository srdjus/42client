# Lacios Mobile Application 

It's still in the early phase. Video presentation of the first version can be found [here](https://www.youtube.com/watch?v=G1oKgQ_M77c).

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/G1oKgQ_M77c" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

## About
The interactive people-meeting application, where people answer each other's questions in real-time. Two individuals connect or send requests to each other, and then a room is created. Once both people enter the room, the game begins. The questions are loaded from a database, along with the possible answers. Users pick questions for their partner, partner responds, and the users choose whether they like the answer or not.

After the game, the percentage of matching answers is displayed, and there is an option to continue chatting.

After starting a server, adjust environmental variables in .env file the run the emulators.